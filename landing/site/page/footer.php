<div class="modal fade" id="masuk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form role="form" action="../../main/log/user_login.php" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title text-center" id="myModalLabel">Log In to Sistem Cerdas Apps</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="text" class="form-control" name="username" placeholder="Username" required>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="password" placeholder="Password" required>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary" name="masuk">Sign In</button>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<h5 class="modal-title text-center" id="myModalLabel">Don't have an account? <a href="#" class="close" data-dismiss="modal" aria-hidden="true"> Sign up »</a></h5>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- jQuery -->    
<script src="vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Plugin JavaScript -->
<script src="js/jquery-ui.min.js" type="text/javascript"></script> 

<script src="js/jquery.easing.min.js"></script>
<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Theme JavaScript -->
<script src="js/creative.min.js"></script>

</body>

</html>