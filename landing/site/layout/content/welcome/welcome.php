<header>
	<div class="header-content">
		<div class="col-md-8">
			<div class="header-content-inner">
				<h1 id="homeHeading">Welcome to Sistem Cerdas Apps</h1>
				<hr>
				<p style="color:#ffffff;">Sistem Cerdas Apps can help you to diagnose and fix it with best suggestion and solutions. Lets go to join <a href="http://localhost/portfolio/FPSP/landing.php?view=home&detail=start">us</a> .</p>
				<a href="" data-toggle="modal" data-target="#masuk" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="login-box-body">
				<p class="login-box-msg">Sign Up to start your session</p>
				<?php
				error_reporting(0);
				$data=$_GET['status'];

				if ($data=='success') { ?>
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check-square-o"></i> Good!</h4>                    
						You successfully registered to Sistem Cerdas Apps.
					</div>
					<?php } elseif ($data=='fail') { ?>
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h4><i class="icon fa fa-ban"></i> Alert!</h4>                  
							Danger alert preview. Your cant sign up at this time.
						</div>
						<?php } else { ?>

							<?php }
							?>
							<?php include_once '../../main/lib/operation/admin/auto.php' ;?>

							<form action="index.php?view=register_user" method="post" enctype="multipart/form-data">

								<input type="hidden" name="id_pakar" name="id_pakar" value="<?php echo $kd;?>">

								<div class="form-group has-feedback">
									<input type="text" class="form-control" placeholder="Name" name="nama" required="">
									<span class="fa fa-user form-control-feedback"></span>
								</div>

								<div class="form-group has-feedback">
									<input type="text" class="form-control" placeholder="Username" name="username" required="">
									<span class="fa fa-user form-control-feedback"></span>
								</div>

								<div class="form-group has-feedback">
									<input type="password" class="form-control" placeholder="Password" name="password" required="">
									<span class="fa fa-lock form-control-feedback"></span>
								</div>

								<div class="form-group has-feedback">
									<input type="password" class="form-control" placeholder="Re-Type Password" name="passwords" required="">
									<span class="fa fa-lock form-control-feedback"></span>
								</div>

								<div class="row">
									<div class="col-xs-8"></div>
									<!-- /.col -->
									<div class="col-xs-4">
										<button type="submit" class="btn btn-primary btn-block btn-flat">Sign Up</button>
									</div>
									<!-- /.col -->
								</div>
							</form>

						</div>
						<!-- /.login-box-body -->
					</div>
				</div>
			</header>