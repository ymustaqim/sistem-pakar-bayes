<?php include_once 'inc/side/side_menu.php' ;?>
<?php
$id = $_GET['id_penyakit'];
$a = "select * from penyakit where id_penyakit = '$id'";
$b = mysql_query($a);
$c = mysql_fetch_array($b);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small>Edit Penyakit</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Edit Penyakit </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-8">
        <!-- /.row -->
        <!-- TABLE: LATEST ORDERS -->
        <!-- general form elements -->
          <!-- TABLE: LATEST ORDERS -->
        <!-- general form elements -->
        <div class="box box-primary" id="place_new_penyakit">
          <div class="box-header with-border">
            <h3 class="box-title fa fa-edit"> Edit Penyakit</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="index.php?view=edit_penyakit_p&id_penyakit=<?php echo $id;?>" method="post" enctype="multipart/form-data">
            <div class="box-body">
              <input type="hidden" name="id_penyakit" value="<?php echo $id;?>">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Penyakit</label>
                <input type="text" name="nama_penyakit" class="form-control" id="exampleInputEmail1" value="<?php echo $c['nama_penyakit'];?>" placeholder="Enter Nama Penyakit">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Latin</label>
                <textarea name="nama_latin" class="form-control" placeholder="Enter Nama Latin"><?php echo $c['nama_latin'];?></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Riwayat Penyakit</label>
                <textarea name="riwayat" class="form-control" placeholder="Enter Riwayat Penyakit"><?php echo $c['riwayat'];?></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Pengendalian</label>
                <textarea name="pengendalian" class="form-control" placeholder="Enter Pengendalian"><?php echo $c['pengendalian'];?></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nilai Populasi</label>
                <input type="text" name="np_populasi" class="form-control" id="exampleInputEmail1" value="<?php echo $c['np_populasi'];?>" placeholder="Enter Nilai Populasi">
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="index.php?view=penyakit&status=cancel_edit"> <span class="btn btn-danger">Cancel</span></a>

            </div>
          </form>
        </div>
        <!-- /.box -->
        <!-- /.box -->
      </div>
      <!-- /.col -->

      <div class="col-md-4">
        <!-- Info Boxes Style 2 -->        
        <!-- PRODUCT LIST -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Recently Added Penyakit</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <ul class="products-list product-list-in-box">
              <?php
              $a ="select * from penyakit order by id_penyakit desc limit 10";
              $b = mysql_query($a);
              while($c = mysql_fetch_array($b)){?>

                <li class="item">
                  <div class="product-img">
                    <img src="dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="index.php?view=edit_penyakit&id_penyakit=<?php echo $c['id_penyakit'];?>" class="product-title"><?php echo $c['nama_penyakit'];?>
                      <span class="label label-warning pull-right">'@<?php echo $c['nama_penyakit'];?>' </span></a>
                      <span class="product-description">
                        '<?php echo $c['id_penyakit'];?>'
                      </span>
                    </div>
                  </li>
                  <?php }?>
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->