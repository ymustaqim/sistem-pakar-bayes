<?php include_once 'inc/side/side_menu.php' ;?>
<?php
$user = $_GET['username'];
$a = "select * from user_pakar where username = '$user'";
$b = mysql_query($a);
$c = mysql_fetch_array($b);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small>Edit Admin</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Edit Admin </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-8">
        <!-- /.row -->
        <!-- TABLE: LATEST ORDERS -->
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title fa fa-edit"> Edit User Pakar</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="index.php?view=edit_admin_p&username=<?php echo $user;?>" method="post" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label >Username</label>
                <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $c['username'];?>" readonly>
              </div>
              <div class="form-group">
                <label  >Old Password</label>
                <input type="text" class="form-control" placeholder="Password"
                value="<?php echo $c['password'];?>" readonly>
              </div>
              <label>Want to change your old password? Type bellow</label>
              <div class="form-group">
                <label  >New Password</label>
                <input type="password" name="password" class="form-control" placeholder="Password">
              </div>
              <div class="form-group">
                <label  >Retype Password</label>
                <input type="password" name="password2" class="form-control" placeholder="Password">
              </div>
               <div class="form-group">
                <label >Nama</label>
                <input type="text" name="nama" class="form-control" placeholder="Username" value="<?php echo $c['nama'];?>">
              </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="index.php?view=user_pakar&status=cancel_edit" type="button" class="btn btn-danger">Cancel</a>

            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->

      <div class="col-md-4">
        <!-- Info Boxes Style 2 -->        
        <!-- PRODUCT LIST -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Recently Added User Pakar</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <ul class="products-list product-list-in-box">
              <?php
              $a ="select * from user_pakar order by username desc limit 10";
              $b = mysql_query($a);
              while($c = mysql_fetch_array($b)){?>

                <li class="item">
                  <div class="product-img">

                  <?php 
                    if ($c['foto']=="null") {?>
                      <img src="dist/img/default.png" alt="Product Image">
                    <?php } else {?>
                      <img src="file/gambar/user/<?php echo $c['foto'];?>" alt="Product Image">
                  <?php } ?>

                    
                  </div>
                  <div class="product-info">
                    <a href="index.php?view=edit_admin&username=<?php echo $c['username'];?>" class="product-title"><?php echo $c['nama'];?>
                      <span class="label label-warning pull-right">'@<?php echo $c['username'];?>' </span></a>
                      <span class="product-description">
                        '<?php echo $c['password'];?>'
                      </span>
                    </div>
                  </li>
                  <?php }?>
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->