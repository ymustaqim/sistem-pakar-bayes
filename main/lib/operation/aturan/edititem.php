<?php include_once 'inc/side/side_menu.php' ;?>
<?php
$id = $_GET['id_aturan'];
$a = "select * from aturan
join gejala on aturan.id_gejala=gejala.id_gejala
join penyakit on aturan.id_penyakit=penyakit.id_penyakit
where id_aturan = '$id'";
$b = mysql_query($a);
$c = mysql_fetch_array($b);
?>

 <script type="text/javascript">
        /*autocomplete muncul setelah user mengetikan minimal2 karakter */
            $(document).ready(function() { 

                $( "#id_penyakit" ).autocomplete({
                 source: "view/auto/data_penyakit.php",  
                   minLength:0,
                   scroll: true,

                   select: function(event,ui){
                    event.preventDefault();
                      $('#id_penyakit').val(ui.item.id_penyakit);
                } 
                });

                $( "#id_gejala" ).autocomplete({
                 source: "view/auto/data_gejala.php",  
                   minLength:0,
                   scroll: true,

                   select: function(event,ui){
                    event.preventDefault();
                      $('#id_gejala').val(ui.item.id_gejala);
                } 
                });

                });
          </script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Dashboard
      <small>Edit Penyakit</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Edit Penyakit </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-8">
        <div class="box box-primary" id="place_new_penyakit">
          <div class="box-header with-border">
            <h3 class="box-title fa fa-edit"> Add Aturan</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="index.php?view=edit_aturan_p&id_aturan=<?php echo $id;?>" method="post" enctype="multipart/form-data">
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Penyakit</label>
                <input type="text" name="id_penyakit" id="id_penyakit" class="form-control" id="exampleInputEmail1" placeholder="Enter Nama Penyakit" value="<?php echo $c['id_penyakit'];?>">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Gejala</label>
                <input type="text" name="id_gejala" id="id_gejala" class="form-control" id="exampleInputEmail1" placeholder="Enter Gejala" value="<?php echo $c['id_gejala'];?>">
              </div>
              <div class="form-group">
                      <label for="exampleInputEmail1">Nilai Probabilitas</label>
                      <input type="text" name="probabilitas" class="form-control" id="exampleInputEmail1" placeholder="Nilai Probabilitas" value="<?php echo $c['probabilitas'];?>">
                    </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="index.php?view=aturan&status=cancel_edit"> <span class="btn btn-danger">Cancel</span></a>

            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->

      <div class="col-md-4">
        <!-- Info Boxes Style 2 -->        
        <!-- PRODUCT LIST -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Recently Added Penyakit</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <ul class="products-list product-list-in-box">
              <?php
              $a ="select * from penyakit order by id_penyakit desc limit 10";
              $b = mysql_query($a);
              while($c = mysql_fetch_array($b)){?>

                <li class="item">
                  <div class="product-img">
                    <img src="dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="index.php?view=edit_penyakit&id_penyakit=<?php echo $c['id_penyakit'];?>" class="product-title"><?php echo $c['nama_penyakit'];?>
                      <span class="label label-warning pull-right">'@<?php echo $c['nama_penyakit'];?>' </span></a>
                      <span class="product-description">
                        '<?php echo $c['id_penyakit'];?>'
                      </span>
                    </div>
                  </li>
                  <?php }?>
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->