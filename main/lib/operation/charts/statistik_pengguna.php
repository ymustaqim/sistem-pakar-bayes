        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title"><span class="fa fa-bar-chart-o"></span> Statistik Pengguna Aplikasi</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive stripp">
              <table class="table table-striped table-hover no-margin" id="example2">
                <tbody>
                  <?php
                  $a ="SELECT * FROM statistik order by hari desc limit 0,5";
                  $b = mysql_query($a);
                  $no =1;
                  while($c = mysql_fetch_array($b)){?>

                    <section class="sidebar">
                      <ul class="sidebar-menu">
                       <li class="treeview">
                        <a href="#">
                          <i class="fa fa-bar-chart-o"></i>
                          <span><?php echo date_format(date_create($c['hari']), 'D,d M Y' );?> at <?php echo $c['jam'];?></span>
                          <span class="label label-info pull-right">
                            <?php if ($c['os']=='Windows 8'|| $c['os']=='Windows 7') { ?>

                              <i class="fa fa-windows"></i> <?php echo $c['os'];?>

                              <?php } elseif ($c['os']=='Mac OS X' || $c['os']=='Mac OS 9' || $c['os']=='iPhone') {?>

                                <i class="fa fa-apple"></i> <?php echo $c['os'];?>

                                <?php }elseif ($c['os']=='Linux' || $c['os']=='Android') {?>
                                 <i class="fa fa-linux"></i> <?php echo $c['os'];?>

                                 <?php} else {?>
                                   <i class="fa fa-question-circle"></i>

                                   <?php } ?>
                                 </span>

                               </a>
                               <ul class="treeview-menu">
                                <li><a href="#"><i class="label label-info">Page</i><p class="pull-right"><?php echo $c['page'];?></p></a></li>
                                <li><a href="#"><i class="label label-info">Browser</i><p class="pull-right"><?php echo $c['browser'];?></p></a></li>
                                <li><a href="#"><i class="label label-info">IP Address</i><p class="pull-right"><?php echo $c['ip'];?></p></a></li>
                                <li><a href="#"><i class="label label-info">Operating System</i><p class="pull-right">
                                  <?php if ($c['os']=='Windows 8'|| $c['os']=='Windows 7') { ?>

                                    <i class="fa fa-windows"></i> <?php echo $c['os'];?>

                                    <?php } elseif ($c['os']=='Mac OS X' || $c['os']=='Mac OS 9' || $c['os']=='iPhone') {?>

                                      <i class="fa fa-apple"></i> <?php echo $c['os'];?>

                                      <?php }elseif ($c['os']=='Linux' || $c['os']=='Android') {?>
                                       <i class="fa fa-linux"></i> <?php echo $c['os'];?>

                                       <?php} else {?>
                                         <i class="fa fa-question-circle"></i> <?php echo $c['os'];?>

                                         <?php } ?>
                                       </p>
                                     </a></li>
                                   </li>
                                 </ul>
                               </section>

                               <?php }?>   

                             </tbody>                   
                           </table>
                         </div>
                         <!-- /.table-responsive -->
                       </div>
                       <!-- /.box-body -->
                       <?php 
                       $data1=mysql_query("select os from statistik where os='Windows 8' or os='Windows 7'"); $win=mysql_num_rows($data1);
                       $data2=mysql_query("select os from statistik where os='Linux' or os='Android'"); $linux=mysql_num_rows($data2);
                       $data3=mysql_query("select os from statistik where os='iPhone' or os='Mac OS x' or os='Mac OS 9' "); $apple=mysql_num_rows($data3);
                       $browser=mysql_query("select browser from statistik"); $browser1=mysql_num_rows($browser);

                       $chrome=mysql_query("SELECT left(browser,13) FROM statistik WHERE left(browser,13) = 'Google Chrome'"); $chrome1=mysql_num_rows($chrome);
                       $Mozilla=mysql_query("SELECT left(browser,16) FROM statistik WHERE left(browser,16) = 'Mozilla Firefox'"); $moz=mysql_num_rows($Mozilla);
                       $saf=mysql_query("SELECT left(browser,12) FROM statistik WHERE left(browser,12) = 'Apple Safari'"); $apple=mysql_num_rows($saf);

                       ?>
                       <div class="box-footer no-padding">
                         <section class="sidebar">
                          <ul class="sidebar-menu">
                           <li class="treeview active">
                            <a href="#">
                              <i class="fa fa-bar-chart-o"> Loading Content of Statistik</i>
                              <span class="pull-right text-red"><i class="fa fa-angle-down"></i></span>
                              <ul class="treeview-menu">
                                <li><a href="#"><i class="label label-info">Operating System</i><p class="pull-right">
                                  <i class="fa fa-windows"></i> <?php echo $win?>&nbsp;/
                                  <i class="fa fa-linux"></i> <?php echo $linux?>&nbsp;/
                                  <i class="fa fa-apple"></i> <?php echo $apple?>&nbsp;
                                </p></a></li>
                                <li><a href="#"><i class="label label-info">Browser</i><p class="pull-right">Chrome <?php echo $chrome1;?> // Mozilla <?php echo $moz;?> // Apple <?php echo $apple;?></p></a></li>
                              </ul>
                            </a>
                          </li>
                        </ul>
                      </section>
                    </div>
                    <div class="box-footer no-padding">
                      <ul class="nav nav-pills nav-stacked">
                        <li><a href="index.php?view=statistik">View all statistik
                          <span class="pull-right text-red"><i class="fa fa-angle-down"></i></span></a>
                        </li>
                      </ul>
                    </div>
                    <!-- /.footer -->
                    <!-- /.footer -->
                  </div>
                  <!-- /.box -->