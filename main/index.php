<?php
session_start();

if (isset($_SESSION['username']) && isset($_SESSION['password']) && isset($_SESSION['level'])) {

    define('qpizza', true);

	require_once 'conf/db.php';
    
    require_once 'inc/page.php';
}

else {
    echo "<script>document.location.href='../landing.php?view=home&detail=start';</script>";
}


?>