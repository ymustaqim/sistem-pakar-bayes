<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <span class="fa fa-home"></span> Dashboard Home
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-red">
          <span class="info-box-icon"><i class="fa fa-users"></i></span>
          <div class="info-box-content">
           <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
           $a = "select * from user_pakar"; $b = mysql_query($a); $rating = mysql_num_rows($b);
           $a1 = "select * from user_pakar where waktu_daftar=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

           ?>
           <span class="progress-description"> <b><?php echo $rating;?></b> Data Pakar</span>
           <span class="progress-description"><?php echo $persen;?> New Data Todays</span>
           <!-- The progress section is optional -->
           <div class="progress progress-sm active">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
          </div>
          <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="fa fa-hospital-o"></i></span>
        <div class="info-box-content">
          <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
          $a = "select * from gejala"; $b = mysql_query($a); $rating = mysql_num_rows($b);
          $a1 = "select * from gejala where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

          ?>
          <span class="progress-description"> <b><?php echo $rating;?></b> Data Gejala</span>           
          <span class="progress-description"><?php echo $persen;?> New Data Todays</span>
          <!-- The progress section is optional -->
          <div class="progress">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
          </div>
          <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-medkit"></i></span>
        <div class="info-box-content">
          <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
          $a = "select * from penyakit"; $b = mysql_query($a); $rating = mysql_num_rows($b);
          $a1 = "select * from penyakit where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

          ?>
          <span class="progress-description"> <b><?php echo $rating;?></b> Data Penyakit</span>
          <span class="progress-description"><?php echo $persen;?> New Data Todays</span>

          <!-- The progress section is optional -->
          <div class="progress">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
          </div>
          <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-yellow">
        <span class="info-box-icon"><i class="fa fa-lock"></i></span>
        <div class="info-box-content">
          <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
          $a = "select * from aturan"; $b = mysql_query($a); $rating = mysql_num_rows($b);
          $a1 = "select * from aturan where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

          ?>
          <span class="progress-description"> <b><?php echo $rating;?></b> Data Aturan</span>
          <span class="progress-description"><?php echo $persen;?> New Data Todays</span>
          <!-- The progress section is optional -->
          <div class="progress">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
          </div>
          <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>
    <!-- /.col -->

  </div>
  <!-- /.row -->


  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-6">
        <!-- TABLE: LATEST ORDERS -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title"><span class="fa fa-medkit"></span> Data Penyakit</h3>
        <div class="box-tools pull-right">
          <?php $a = "select * from penyakit"; $b = mysql_query($a); $pengguna = mysql_num_rows($b); ?>

          <span class="label label-danger"><?php echo $pengguna;?> Penyakit</span>
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
       <div class="table-responsive dataTable_wrapper">
        <table class="table no-margin table-striped" id="example2">
          <thead>
            <tr>
              <th>No.</th>
              <th>ID Penyakit</th>
              <th>Nama Penyakit</th>
              <th>Populasi</th>
            </tr>
          </thead>
          <tbody>

            <?php
            $a ="SELECT * FROM penyakit";
            $b = mysql_query($a);
            $no =1;
            while($c = mysql_fetch_array($b)){?>
              <tr>
                <td><?php echo $no;?></td>
                <td><a href="#"><b><?php echo $c['id_penyakit'];?></b></a></td>
                <td><b><?php echo $c['nama_penyakit'];?></b></td>
                <td><b><?php echo $c['np_populasi'];?></b></td>
              </tr>
              <?php
              $no++;
            }
            ?>


          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
   <div class="box-footer no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="index.php?view=penyakit">Lihat
                <span class="pull-right text-red"><i class="fa fa-angle-down"></i></span></a>
              </li>
            </ul>
          </div>
    <!-- /.box-footer -->
  </div>
  <!-- /.box -->
      <!-- /.box -->
      <!-- TABLE: LATEST ORDERS -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title"><span class="fa fa-hospital-o"></span> Data Gejala</h3>
          <div class="box-tools pull-right">
            <?php $a = "select * from gejala"; $b = mysql_query($a); $pengguna = mysql_num_rows($b); ?>

            <span class="label label-danger"><?php echo $pengguna;?> Gejala</span>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
         <div class="table-responsive dataTable_wrapper">
          <table class="table no-margin table-striped" id="example1">
            <thead>
              <tr>
                <th>No.</th>
                <th>ID</th>
                <th>Nama Gejala</th>
              </tr>
            </thead>
            <tbody>

              <?php
              $a ="SELECT * FROM gejala";
              $b = mysql_query($a);
              $no =1;
              while($c = mysql_fetch_array($b)){?>
                <tr>
                  <td><?php echo $no;?></td>
                  <td><a href="#"><b><?php echo $c['id_gejala'];?></b></a></td>
                  <td><b><?php echo $c['nama_gejala'];?></b></td>
                </tr>
                <?php
                $no++;
              }
              ?>


            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.box-body -->
     <div class="box-footer no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="index.php?view=gejala">Lihat
                <span class="pull-right text-red"><i class="fa fa-angle-down"></i></span></a>
              </li>
            </ul>
          </div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</div>
<!-- /.col -->

<!-- DATA STATISTIK -->
<div class="col-md-6">

<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title"><span class="fa fa-users"></span> Data Aturan</h3>
       <div class="box-tools pull-right">
        <?php $a = "select * from aturan"; $b = mysql_query($a); $pengguna = mysql_num_rows($b); ?>

        <span class="label label-danger"><?php echo $pengguna;?> Aturan</span>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
        </button>
      </div>
    </div>

     <div class="box-body">
       <div class="table-responsive dataTable_wrapper">
        <table class="table no-margin table-striped" id="example3">
          <thead>
            <tr>
              <th>No.</th>
              <th>ID Aturan</th>
              <th>ID Penyakit</th>
              <th>ID Gejala</th>
              <th>probabilitas</th>
            </tr>
          </thead>
          <tbody>

            <?php
            $a ="SELECT * FROM aturan order by id_aturan desc limit 40";
            $b = mysql_query($a);
            $no =1;
            while($c = mysql_fetch_array($b)){?>
              <tr>
                <td><?php echo $no;?></td>
                <td><a href="#"><b><?php echo $c['id_aturan'];?></b></a></td>
                <td><b><?php echo $c['id_penyakit'];?></b></td>
                <td><b><?php echo $c['id_gejala'];?></b></td>
                <td><b><?php echo $c['probabilitas'];?></b></td>
              </tr>
              <?php
              $no++;
            }
            ?>


          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->

     <div class="box-footer no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="index.php?view=aturan">Lihat
                <span class="pull-right text-red"><i class="fa fa-angle-down"></i></span></a>
              </li>
            </ul>
          </div>
  </div>


  <!-- USERS LIST -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title"><span class="fa fa-users"></span> Data Member</h3>

      <div class="box-tools pull-right">
        <?php $a = "select * from user_pakar"; $b = mysql_query($a); $pengguna = mysql_num_rows($b); ?>

        <span class="label label-danger"><?php echo $pengguna;?> Members</span>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
        </button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
      <ul class="users-list clearfix">
        <?php $a = "select * from user_pakar order by username desc limit 12"; $b = mysql_query($a); while($pengguna = mysql_fetch_array($b)){ ?>
          <li>
            <?php if ($pengguna['foto']=='null') {?>
              <img src="dist/img/default.png" alt="<?php echo $pengguna['username'];?>">
              <?php } else { ?>
                <img src="file/gambar/user/<?php echo $pengguna['foto'];?>" alt="<?php echo $pengguna['username'];?>" width="50" height="50">
                <?php } ?>
                

                <a class="users-list-name" href="#"><?php echo $pengguna['username'];?></a>
                <span class="users-list-date">'<?php echo $pengguna['id_pakar'];?>'</span>
              </li>
              <?php }?>

            </ul>
            <!-- /.users-list -->
          </div>
          <!-- /.box-body -->
          <!-- /.box-body -->
          <div class="box-footer no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="index.php?view=user_pakar">Lihat
                <span class="pull-right text-red"><i class="fa fa-angle-down"></i></span></a>
              </li>
            </ul>
          </div>
          <!-- /.box-footer -->
          <!-- /.box-footer -->
        </div>
        <!--/.box -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<script> $(function () { $("#example1").DataTable(); }); </script>
<script> $(function () { $("#example2").DataTable(); }); </script>
<script> $(function () { $("#example3").DataTable(); }); </script>