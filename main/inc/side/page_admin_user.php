<?php $id="spff"; $token=md5($id); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <span class="fa fa-home"></span> Dashboard Home
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-red">
          <span class="info-box-icon"><i class="fa fa-users"></i></span>
          <div class="info-box-content">
           <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
           $a = "select * from user_pakar"; $b = mysql_query($a); $rating = mysql_num_rows($b);
           $a1 = "select * from user_pakar where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

           ?>
           <span class="progress-description"> <b><?php echo $rating;?></b> Data Pakar</span>
           <span class="progress-description"><?php echo $persen;?> New Data Todays</span>
           <!-- The progress section is optional -->
           <div class="progress progress-sm active">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
          </div>
          <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="fa fa-hospital-o"></i></span>
        <div class="info-box-content">
          <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
          $a = "select * from gejala"; $b = mysql_query($a); $rating = mysql_num_rows($b);
          $a1 = "select * from gejala where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

          ?>
          <span class="progress-description"> <b><?php echo $rating;?></b> Data Gejala</span>           
          <span class="progress-description"><?php echo $persen;?> New Data Todays</span>
          <!-- The progress section is optional -->
          <div class="progress">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
          </div>
          <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-medkit"></i></span>
        <div class="info-box-content">
          <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
          $a = "select * from penyakit"; $b = mysql_query($a); $rating = mysql_num_rows($b);
          $a1 = "select * from penyakit where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

          ?>
          <span class="progress-description"> <b><?php echo $rating;?></b> Data Penyakit</span>
          <span class="progress-description"><?php echo $persen;?> New Data Todays</span>

          <!-- The progress section is optional -->
          <div class="progress">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
          </div>
          <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-yellow">
        <span class="info-box-icon"><i class="fa fa-lock"></i></span>
        <div class="info-box-content">
          <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
          $a = "select * from aturan"; $b = mysql_query($a); $rating = mysql_num_rows($b);
          $a1 = "select * from aturan where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

          ?>
          <span class="progress-description"> <b><?php echo $rating;?></b> Data Aturan</span>
          <span class="progress-description"><?php echo $persen;?> New Data Todays</span>
          <!-- The progress section is optional -->
          <div class="progress">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
          </div>
          <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
          </span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div>
    <!-- /.col -->

  </div>
  <!-- /.row -->


  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-8">
      <!-- /.box -->

      <!-- USERS LIST -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title"><span class="fa fa-users"></span> Data Member Terakhir</h3>

          <div class="box-tools pull-right">
            <?php $a = "select * from user_pakar"; $b = mysql_query($a); $pengguna = mysql_num_rows($b); ?>

            <span class="label label-danger"><?php echo $pengguna;?> Members</span>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
            </button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <ul class="users-list clearfix">
            <?php $a = "select * from user_pakar order by username desc limit 8"; $b = mysql_query($a); while($pengguna = mysql_fetch_array($b)){ ?>
              <li>
                <img src="dist/img/default.png" alt="<?php echo $pengguna['username'];?>">
                <a class="users-list-name" href="#"><?php echo $pengguna['username'];?></a>
                <span class="users-list-date">'<?php echo $pengguna['id_pakar'];?>'</span>
              </li>
              <?php }?>

            </ul>
            <!-- /.users-list -->
          </div>
          <!-- /.box-body -->
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            <a href="index.php?view=user_pakar&status=place_new_member#place_new_member" class="btn btn-sm btn-info btn-flat pull-left">Tambah</a>
            <a href="index.php?view=user_pakar&status=view_all_member#view_all_member" class="btn btn-sm btn-default btn-flat pull-right">Lihat</a>
          </div>
          <!-- /.box-footer -->
          <!-- /.box-footer -->
        </div>
        <!--/.box -->

        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title"><span class="fa fa-hospital-o"></span> Data Gejala Terakhir</h3>
            <div class="box-tools pull-right">
              <?php $a = "select * from gejala"; $b = mysql_query($a); $pengguna = mysql_num_rows($b); ?>

              <span class="label label-danger"><?php echo $pengguna;?> Gejala</span>
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
           <div class="table-responsive dataTable_wrapper">
            <table class="table no-margin table-striped" id="example1">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Gejala ID</th>
                  <th>Nama</th>
                </tr>
              </thead>
              <tbody>

                <?php
                $a ="SELECT * FROM gejala";
                $b = mysql_query($a);
                $no =1;
                while($c = mysql_fetch_array($b)){?>
                  <tr>
                    <td><?php echo $no;?></td>
                    <td><a href="#"><b><?php echo $c['id_gejala'];?></b></a></td>
                    <td><b><?php echo $c['nama_gejala'];?></b></td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>


              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="index.php?view=gejala&status=place_new_gejala#place_new_gejala" class="btn btn-sm btn-info btn-flat pull-left">Tambah</a>
          <a href="index.php?view=gejala&status=view_all_gejala#view_all_gejala" class="btn btn-sm btn-default btn-flat pull-right">Lihat</a>
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->

      <!-- TABLE: LATEST ORDERS -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title"><span class="fa fa-medkit"></span> Data Penyakit Terakhir</h3>
          <div class="box-tools pull-right">
            <?php $a = "select * from penyakit"; $b = mysql_query($a); $pengguna = mysql_num_rows($b); ?>

            <span class="label label-danger"><?php echo $pengguna;?> Penyakit</span>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
         <div class="table-responsive dataTable_wrapper">
          <table class="table no-margin table-striped" id="example2">
            <thead>
              <tr>
                <th>No.</th>
                <th>Penyakit ID</th>
                <th>Nama</th>
                <th>Populasi</th>
              </tr>
            </thead>
            <tbody>

              <?php
              $a ="SELECT * FROM penyakit";
              $b = mysql_query($a);
              $no =1;
              while($c = mysql_fetch_array($b)){?>
                <tr>
                  <td><?php echo $no;?></td>
                  <td><a href="#"><b><?php echo $c['id_penyakit'];?></b></a></td>
                  <td><b><?php echo $c['nama_penyakit'];?></b></td>
                  <td><b><?php echo $c['np_populasi'];?></b></td>
                </tr>
                <?php
                $no++;
              }
              ?>


            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <a href="index.php?view=gejala&status=place_new_gejala#place_new_gejala" class="btn btn-sm btn-info btn-flat pull-left">Tambah</a>
        <a href="index.php?view=gejala&status=view_all_gejala#view_all_gejala" class="btn btn-sm btn-default btn-flat pull-right">Lihat</a>
      </div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->

  <!-- DATA STATISTIK -->
  <div class="col-md-4">

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Browser Usage</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <div class="chart-responsive">
              <canvas id="pieChart" height="150"></canvas>
            </div>
            <!-- ./chart-responsive -->
          </div>
          <!-- /.col -->
          <div class="col-md-4">
            <ul class="chart-legend clearfix">
              <li><i class="fa fa-circle-o text-red"></i> Chrome</li>
              <li><i class="fa fa-circle-o text-green"></i> IE</li>
              <li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
              <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
              <li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
              <li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
            </ul>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
      <div class="box-footer no-padding">
        <ul class="nav nav-pills nav-stacked">
          <li><a href="index.php?view=statistik&spff=<?php echo $token;?>">View all statistik
            <span class="pull-right text-red"><i class="fa fa-angle-down"></i></span></a>
          </li>
        </ul>
      </div>
      <!-- /.footer -->
    </div>
    <!-- /.box -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title"><span class="fa fa-bar-chart-o"></span> Statistik Pengguna Aplikasi</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive stripp">
          <table class="table table-striped table-hover no-margin" id="example2">
            <tbody>
              <?php
              $a ="SELECT * FROM statistik order by hari desc limit 0,10";
              $b = mysql_query($a);
              $no =1;
              while($c = mysql_fetch_array($b)){?>

                <section class="sidebar">
                  <ul class="sidebar-menu">
                   <li class="treeview">
                    <a href="#">
                      <i class="fa fa-bar-chart-o"></i>
                      <span><?php echo date_format(date_create($c['hari']), 'D,d M Y' );?> at <?php echo $c['jam'];?></span>
                      <span class="label label-info">
                        <?php if ($c['os']=='Windows 8'|| $c['os']=='Windows 7') { ?>

                          <i class="fa fa-windows"></i> <?php echo $c['os'];?>

                          <?php } elseif ($c['os']=='Mac OS X' || $c['os']=='Mac OS 9' || $c['os']=='iPhone') {?>

                            <i class="fa fa-apple"></i> <?php echo $c['os'];?>

                             <?php }elseif ($c['os']=='Linux' || $c['os']=='Android') {?>
                               <i class="fa fa-linux"></i> <?php echo $c['os'];?>

                               <?php} else {?>
                                 <i class="fa fa-question-circle"></i>

                                 <?php } ?>
                               </span>

                             </a>
                             <ul class="treeview-menu">
                              <li><a href="#"><i class="label label-info">Page</i><p class="pull-right"><?php echo $c['page'];?></p></a></li>
                              <li><a href="#"><i class="label label-info">Browser</i><p class="pull-right"><?php echo $c['browser'];?></p></a></li>
                              <li><a href="#"><i class="label label-info">IP Address</i><p class="pull-right"><?php echo $c['ip'];?></p></a></li>
                              <li><a href="#"><i class="label label-info">Operating System</i><p class="pull-right">
                                <?php if ($c['os']=='Windows 8'|| $c['os']=='Windows 7') { ?>

                                  <i class="fa fa-windows"></i> <?php echo $c['os'];?>

                                  <?php } elseif ($c['os']=='Mac OS X' || $c['os']=='Mac OS 9' || $c['os']=='iPhone') {?>

                                    <i class="fa fa-apple"></i> <?php echo $c['os'];?>

                                     <?php }elseif ($c['os']=='Linux' || $c['os']=='Android') {?>
                                       <i class="fa fa-linux"></i> <?php echo $c['os'];?>

                                       <?php} else {?>
                                         <i class="fa fa-question-circle"></i> <?php echo $c['os'];?>

                                         <?php } ?>
                                       </p>
                                     </a></li>
                                   </li>
                                 </ul>
                               </section>

                               <?php }?>   

                             </tbody>                   
                           </table>
                         </div>
                         <!-- /.table-responsive -->
                       </div>
                       <!-- /.box-body -->
                       <?php 
                       $data1=mysql_query("select os from statistik where os='Windows 8' or os='Windows 7'"); $win=mysql_num_rows($data1);
                       $data2=mysql_query("select os from statistik where os='Linux' or os='Android'"); $linux=mysql_num_rows($data2);
                       $data3=mysql_query("select os from statistik where os='iPhone' or os='Mac OS x' or os='Mac OS 9' "); $apple=mysql_num_rows($data3);
                       $browser=mysql_query("select browser from statistik"); $browser1=mysql_num_rows($browser);

                       $chrome=mysql_query("SELECT left(browser,13) FROM statistik WHERE left(browser,13) = 'Google Chrome'"); $chrome1=mysql_num_rows($chrome);
                       $Mozilla=mysql_query("SELECT left(browser,13) FROM statistik WHERE left(browser,13) = 'Mozilla'"); $moz=mysql_num_rows($Mozilla);
                       $saf=mysql_query("SELECT left(browser,13) FROM statistik WHERE left(browser,13) = 'Apple Safari'"); $apple=mysql_num_rows($saf);

                       ?>
                       <div class="box-footer no-padding">
                         <section class="sidebar">
                          <ul class="sidebar-menu">
                           <li class="treeview active">
                            <a href="#">
                              <i class="fa fa-bar-chart-o"> Loading Content of Statistik</i>
                              <span class="pull-right text-red"><i class="fa fa-angle-down"></i></span>
                              <ul class="treeview-menu">
                                <li><a href="#"><i class="label label-info">Operating System</i><p class="pull-right">
                                  <i class="fa fa-windows"></i> <?php echo $win?>&nbsp;/
                                  <i class="fa fa-linux"></i> <?php echo $linux?>&nbsp;/
                                  <i class="fa fa-apple"></i> <?php echo $apple?>&nbsp;
                                </p></a></li>
                                <li><a href="#"><i class="label label-info">Browser</i><p class="pull-right">Chrome <?php echo $chrome1;?> // Mozilla <?php echo $moz;?> // Apple <?php echo $apple;?></p></a></li>
                              </ul>
                            </a>
                          </li>
                        </ul>
                      </section>
                    </div>
                    <div class="box-footer no-padding">
                      <ul class="nav nav-pills nav-stacked">
                        <li><a href="index.php?view=statistik&spff=<?php echo $token;?>">View all statistik
                          <span class="pull-right text-red"><i class="fa fa-angle-down"></i></span></a>
                        </li>
                      </ul>
                    </div>
                    <!-- /.footer -->
                    <!-- /.footer -->
                  </div>
                  <!-- /.box -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </section>
            <!-- /.content -->
          </div>
          <script> $(function () { $("#example1").DataTable(); }); </script>
          <script> $(function () { $("#example2").DataTable(); }); </script>