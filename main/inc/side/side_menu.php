<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
       <?php
        $user = $_SESSION['id_pakar'];
        
        $a = "select * from user_pakar where id_pakar = '$user'";
        $b = mysql_query($a);
        $c = mysql_fetch_array($b);
        ?>

      <div class="user-panel">
        <div class="pull-left image">
        <?php if ($c['foto']=='null') {?>
          <img src="dist/img/default.png" class="img-circle" alt="User Image">          
        <?php } else { ?>
          <img src="file/gambar/user/<?php echo $c['foto'];?>" class="img-circle" alt="User Image">
          <?php } ?>
        </div>
        <div class="pull-left info">
          <p><a href="index.php?view=profile"><?php echo $c['nama'];?></a></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="treeview">
          <a href="index.php?view=home">
            <i class="fa fa-home"></i> <span>Home</span></i>
            <span class="label label-primary pull-right">Home</span>
          </a>
        </li>
         <li class="treeview">
          <a href="index.php?view=timeline">
            <i class="fa fa-list-alt"></i> <span>Timeline</span></i>
            <span class="label label-primary pull-right">Timeline</span>
          </a>
        </li>
        <li class="treeview">
          <a href="index.php?view=user_pakar">
            <i class="fa fa-users"></i> <span>Member</span></i>
            <?php $a = "select * from user_pakar"; $b = mysql_query($a); $c = mysql_num_rows($b); ?>

            <span class="label label-info pull-right"><?php echo $c;?></span>
          </a>
        </li>
        <li class="treeview">
          <a href="index.php?view=gejala">
            <i class="fa fa-hospital-o"></i>
            <span>Gejala</span>
            <?php $a = "select * from gejala"; $b = mysql_query($a); $cabang = mysql_num_rows($b); ?>
            <span class="label label-info pull-right"><?php echo $cabang;?></span>
          </a>
        </li>
         <li class="treeview">
          <a href="index.php?view=penyakit">
            <i class="fa fa-medkit"></i>
            <span>Penyakit</span>
            <?php $a = "select * from penyakit"; $b = mysql_query($a); $cabang = mysql_num_rows($b); ?>
            <span class="label label-info pull-right"><?php echo $cabang;?></span>
          </a>
        </li>
         <li class="treeview">
          <a href="index.php?view=aturan">
            <i class="fa fa-lock"></i>
            <span>Aturan</span>
            <?php $a = "select * from aturan"; $b = mysql_query($a); $cabang = mysql_num_rows($b); ?>
            <span class="label label-info pull-right"><?php echo $cabang;?></span>
          </a>
        </li>
         <li class="treeview">
          <a href="index.php?view=diagnosa">
            <i class="fa fa-check-square-o"></i>
            <span>Diagnosa</span>
            <span class="label label-success pull-right"><i>diagnosa</i></span>
          </a>
        </li>
         <li class="treeview">
          <a href="index.php?view=laporan">
            <i class="fa fa-file-word-o"></i>
            <span>Laporan</span>
            <span class="label label-warning pull-right"><i>Laporan</i></span>
          </a>
        </li>
         <li class="treeview">
          <a href="index.php?view=signout>" onclick="return confirm('Are you sure?')">
            <i class="fa fa-sign-out"></i>
            <span>Signout</span>
            <span class="label label-danger pull-right"><i>signout</i></span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>