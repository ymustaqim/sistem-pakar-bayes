<footer class="main-footer">
    <div class="pull-right hidden-xs fixed-top">
      <b>Version</b> 0.0.1
    </div>
    <?php $th=date('Y'); ?>
    <strong>Copyright &copy; <?php echo $th;?> <a href="#" target="_blank">Sistem Cerdas Apps</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- <script src="bootstrap/js/metismenu.js"></script> -->
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="plugins/pace/pace.min.js"></script>

<script src="plugins/chartjs/Chart.min.js"></script>
<script src="dist/js/pages/browser.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="dist/js/app.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script src="plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script> 

  

</body>
</html>