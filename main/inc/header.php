<?php
//error_reporting(0);
require_once '../main/view/statistik/functions.php';

$ip      = ip_user();
$browser = browser_user();
$os      = os_user();
$user    = $_SESSION['id_pakar'];
if (strpos($browser, 'Chrome') !== false) {
  $color = '#f56954';
} elseif (strpos($browser, 'Internet') !== false) {
  $color = '#00a65a';
} elseif (strpos($browser, 'Mozilla') !== false){
  $color = '#f39c12';
} elseif (strpos($browser, 'Safari') !== false){
  $color = '#00c0ef';
} elseif (strpos($browser, 'Opera') !== false){
  $color = '#3c8dbc';
} else {
  $color = '#d2d6de';
}


// Check bila sebelumnya data pengunjung sudah terrekam
if (!isset($_COOKIE['VISITOR'])) {

    // Masa akan direkam kembali
    // Tujuan untuk menghindari merekam pengunjung yang sama dihari yang sama.
    // Cookie akan disimpan selama 24 jam
  $duration = time()+60*60*24;

    // simpan kedalam cookie browser
  setcookie('VISITOR',$browser,$duration);

    // SQL Command atau perintah SQL INSERT
  $sql = "INSERT INTO statistik (ip, os, browser, id_pakar,hari,jam,color,highligt) VALUES ('$ip', '$os', '$browser','$user',curdate(), curtime(),'$color','$color')";

    // variabel { $db } adalah perwakilan dari koneksi database lihat config.php
  $query = mysql_query($sql);
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistem Cerdas Apps | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bootstrap/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bootstrap/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" type="text/css" href="plugins/datepicker/jquery.datetimepicker.css">
  <link rel="stylesheet" href="plugins/pace/pace.min.css">
  
  <script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <script src="plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script>   

<script src="plugins/datepicker/jquery.datetimepicker.full.js"></script>

</head>
<?php 
if ($_SESSION['level']=='user') {?>
<body class="hold-transition skin-blue sidebar-mini pace-done">
<?php } else {?>
<body class="hold-transition skin-blue sidebar-mini pace-done">
<?php }
?>
<div class="pace pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>

<div class="wrapper">
  <header class="main-header">

    <!-- Logo -->

    <a href="index.php?view=home&action=rollback" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>C</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Sistem Cerdas </b>Apps</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-fixed-top">
      <!-- Sidebar toggle button-->
      <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a> -->
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <?php
              $user = $_SESSION['id_pakar'];
              $level = $_SESSION['level'];

              $a ="select * from user_pakar where id_pakar='$user' and level='$level' ";
              $b = mysql_query($a);
              $c = mysql_fetch_array($b);
              ?>
          <li class="dropdown user user-menu">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown">
             <?php if ($c['foto']=='null') {?>
              <img class="user-image" src="dist/img/default.png" alt="<?php echo $c['username'];?>">
             <?php } else { ?>
              <img class="user-image" src="file/gambar/user/<?php echo $c['foto'];?>" alt="<?php echo $c['username'];?>">
              <?php } ?>
              <span class="hidden-xs"><?php echo $c['nama'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li>
               <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <?php if ($c['cover']=='null') {?>
          <div class="widget-user-header bg-black" style="background: url('dist/img/photo1.png') center center;">

          <?php } else { ?>
          <div class="widget-user-header bg-black" style="background: url('file/gambar/cover/<?php echo $c['cover'];?>') center center;">
          <?php } ?>
              <h3 class="widget-user-username"><?php echo $c['nama'];?></h3>
              <h5 class="widget-user-desc">Web Designer</h5>
            </div>
            <div class="widget-user-image">
            <?php if ($c['foto']=='null') {?>
              <img class="img-circle" src="dist/img/default.png" alt="<?php echo $c['username'];?>">
              
            <?php } else { ?>
              <img class="img-circle" src="file/gambar/user/<?php echo $c['foto'];?>" alt="<?php echo $c['username'];?>">
            <?php } ?>
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-6 border-right">
                  <div class="description-block">
                    <span class="description-text">Member since</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-6 border-right">
                  <div class="description-block">
                    <span class="description-text"><span align="center" class="badge bg-aqua"><?php echo date_format(date_create($c['waktu_daftar']), 'M Y' );?></span></span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.widget-user -->
              </li>
              <li class="user-footer">
                <div class="pull-left">
                <?php 
                if ($_SESSION['level']=='admin') {?>
                  <a href="index.php?view=profile" class="btn btn-info btn-flat">Profile</a>
                <?php } else { ?>
                  <a href="index.php?view=p768sdhfksdKJGHKLJGHKLH768djhkskJH" class="btn btn-info btn-flat">Profile</a>                  
                <?php }?>
                </div>
                <div class="pull-right">
                  <a href="index.php?view=signout" onclick="return confirm('Are you sure?')" class="btn btn-warning btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
           <li>
            <a href="#" data-toggle="control-sidebar"><i class=""></i></a>
          </li>         
        </ul>
      </div>

    </nav>
  </header>

  <?php include_once 'module.php'; ?>
  
  <style type="text/css"> ul.ui-autocomplete.ui-menu{ display: inline-block; background-color: aqua;  } </style>