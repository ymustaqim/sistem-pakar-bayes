<?php 
if ($_SESSION['level']=='admin') { ?>
	<?php include_once 'inc/side/side_menu.php' ;?>
	<?php include_once 'lib/operation/diagnosa/auto.php' ;?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				<span class="fa fa-check-square-o"></span> Dashboard Diagnosa
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Diagnosa</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<!-- Info boxes -->
			<!-- Main row -->
			<div class="row">
				<!-- Left col -->
				<div class="col-md-8">
					 <div class="callout callout-info">
                    <h4 class="fa fa-info"> Choose one of data below.</h4>
                </div> 	
					<?php
					error_reporting(0);
					$data=$_GET['status'];

					if ($data=='success') { ?>
						<div class="alert alert-success alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h4><i class="icon fa fa-check-square-o"></i> Good!</h4>	                
							You successfully add all data to the Apps.
						</div>
						<?php } elseif ($data=='fail') { ?>
							<div class="alert alert-danger alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-ban"></i> Alert!</h4>	                
								Danger alert preview. This alert is dismissable. You can't let the form is null or no data.
							</div>
							<?php } else { ?>

								<?php }
								?>

								<div class="box box-info" id="view_all_member">
									<div class="box-header">
										<h3 class="box-title">Data Table Diagnosa</h3>
									</div>
									<!-- /.box-header -->
									<form role="form" action="index.php?view=check&status=store_to_checking&id_diagnosa=<?php echo $kd;?>" method="post" enctype="multipart/form-data">
										<input type="hidden" name="id_pakar" value="<?php echo $_SESSION['id_pakar'];?>"></input>
										<input type="hidden" name="id_diagnosa" value="<?php echo $kd;?>"></input>
										<div class="box-body">
											<div class="table-responsive dataTable_wrapper">

												<table class="table table-bordered table-striped">
													<thead>
														<tr>
															<th>Nomer</th>
															<th>ID Gejala</th>
															<th>Gejala Penyakit</th>
															<th>Check</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<?php
															$a ="select * from aturan
															join penyakit on aturan.id_penyakit=penyakit.id_penyakit
															join gejala on aturan.id_gejala=gejala.id_gejala group by gejala.nama_gejala";
															$b = mysql_query($a);
															$no =1;

															while($c = mysql_fetch_array($b)){												
																?>
																<td><?php echo $no;?></td>													
																<td><a href="#"><b><?php echo $c['id_gejala'];?></b></a></td>
																<td><?php echo $c['nama_gejala']; ?></td>
																<td>
																	<input type="checkbox" name="id_aturan[]" value="
																	<?php 
																	$aa =mysql_query("select id_aturan from aturan
																		join penyakit on aturan.id_penyakit=penyakit.id_penyakit
																		join gejala on aturan.id_gejala=gejala.id_gejala 
																		where gejala.id_gejala='$c[id_gejala]'");
																	//$data_aturan = mysql_fetch_array($aa);

																	while($bb = mysql_fetch_array($aa)) {
																		echo $bb['id_aturan'].',';
																	} ?> ">																	
																	</input>
																</td>
															</tr>
															<?php $no++; } ?>
													</tbody>
													<tfoot>
														<tr>
															<th>
																<button type="submit" class="btn btn-info">Submit</button>
															</th>
														</tr>
													</tfoot>							
												</table>
												
											</div>
										</div>
									</form>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<!-- /.col -->

							<div class="col-md-4">
								<!-- Info Boxes Style 2 -->        
								<!-- PRODUCT LIST -->
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title">Recently Added Penyakit</h3>

										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
											</button>
											<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
										</div>
									</div>
									<!-- /.box-header -->
									<div class="box-body">
										<ul class="products-list product-list-in-box">
											<?php
											$a ="select * from penyakit order by id_penyakit desc limit 10";
											$b = mysql_query($a);
											while($c = mysql_fetch_array($b)){?>

												<li class="item">
													<div class="product-img">
														<img src="dist/img/default-50x50.gif" alt="Product Image">
													</div>
													<div class="product-info">
														<a href="index.php?view=edit_penyakit&id_penyakit=<?php echo $c['id_penyakit'];?>" class="product-title"><?php echo $c['nama_penyakit'];?>
															<span class="label label-warning pull-right"><?php echo date_format(date_create($c['waktu']),'D, d M Y');?> </span></a>
															<span class="product-description">
																'<?php echo $c['id_penyakit'];?>'
															</span>
														</div>
													</li>
													<?php }?>
													<!-- /.item -->
												</ul>
											</div>
											<!-- /.box-body -->
											<!-- /.box-footer -->
										</div>
										<!-- /.box -->
									</div>
									<!-- /.col -->

								</div>
								<!-- /.row -->
							</section>
							<!-- /.content -->
						</div>
						<!-- /.content-wrapper -->





						<?php } else { ?>

							<?php include_once 'inc/side/side_menu_user.php' ;?>

							<div class="content-wrapper">
								<!-- Content Header (Page header) -->
								<!-- Content Header (Page header) -->
								<section class="content-header">
									<h1>
										<span class="fa fa-lock"></span> Dashboard Aturan
									</h1>
									<ol class="breadcrumb">
										<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
										<li class="active">Aturan</li>
									</ol>
								</section>
								<!-- Main content -->
								<section class="content">
									<!-- Info boxes -->
									<!-- Main row -->
									<div class="row">
										<!-- Left col -->
										<div class="col-md-8">

											<div class="error-page">
												<h2 class="headline text-yellow"> 404</h2>

												<div class="error-content">
													<h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

													<p>
														We could not find the page you were looking for.
														Meanwhile, you may <a href="index.php?view=home&status=not_found_page">return to HOME</a> or try using the search form.
													</p>

													<form class="search-form">
														<div class="input-group">
															<input type="text" name="search" class="form-control" placeholder="Search">

															<div class="input-group-btn">
																<button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
																</button>
															</div>
														</div>
														<!-- /.input-group -->
													</form>
												</div>
												<!-- /.error-content -->
											</div>
											<!-- /.error-page -->
										</div>
									</div>
								</section>
							</div>

							<?php } ?>
