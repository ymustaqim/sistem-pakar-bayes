	<?php 
	if ($_SESSION['level']=='admin') { ?>

		<?php include_once 'inc/side/side_menu.php' ;?>
		<?php include_once 'lib/operation/aturan/auto.php' ;?>

		<?php
		function penyakit(){
			$q = "SELECT * FROM `penyakit` ORDER BY 'id_penyakit' ASC";
			$sql = mysql_query($q);
			while($res = mysql_fetch_array($sql)){
				echo "<option value='".$res[0]."'>".$res['nama_penyakit']."</option>";
			}
		}

		function gejala(){
			$q = "SELECT * FROM `gejala` ORDER BY 'id_gejala' ASC";
			$sql = mysql_query($q);
			while($res = mysql_fetch_array($sql)){
				echo "<option value='".$res[0]."'>".$res['nama_gejala']."</option>";
			}
		}

		?>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					<span class="fa fa-lock"></span> Dashboard Aturan
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Aturan</li>
				</ol>
			</section>
			<!-- Main content -->
			<section class="content">
				<!-- Info boxes -->
				<!-- Main row -->
				<div class="row">
					<!-- Left col -->
					<div class="col-md-8">
						<?php
						error_reporting(0);
						$data=$_GET['status'];

						if ($data=='success') { ?>
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-check-square-o"></i> Good!</h4>	                
								You successfully add all data to the Apps.
							</div>
							<?php } elseif ($data=='fail') { ?>
								<div class="alert alert-danger alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<h4><i class="icon fa fa-ban"></i> Alert!</h4>	                
									Danger alert preview. This alert is dismissable. You can't let the form is null or no data.
								</div>
								<?php } else { ?>

									<?php }
									?>
									
									<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

										<div class="modal-dialog">
											<div class="box box-primary" id="place_new_penyakit">
												<div class="box-header with-border">
													<h3 class="box-title fa fa-plus-circle"> Edit Aturan</h3>
												</div>
												<!-- /.box-header -->
												<!-- form start -->
												<form role="form" action="index.php?view=add_aturan" method="post" enctype="multipart/form-data">
													<div class="box-body">
														<input type="hidden" name="id_aturan" value="<?php echo $kd;?>">
														<div class="form-group">
															<label for="exampleInputEmail1">Nama Penyakit</label>
															<select name="id_penyakit" class="form-control"><?php penyakit();?></select>
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Gejala</label>
															<select name="id_gejala" class="form-control"><?php gejala();?></select>

														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Nilai Probabilitas</label>
															<input type="text" name="probabilitas" class="form-control" id="exampleInputEmail1" placeholder="Nilai Probabilitas">
														</div>
													</div>
													<!-- /.box-body -->

													<div class="box-footer">
														<button type="submit" class="btn btn-primary">Submit</button>
														<button type="button" class="btn btn-warning pull-right" data-dismiss="modal">Close</button>

													</div>
												</form>
											</div>
											<!-- /.box -->
										</div>
									</div>


									<div class="box" id="view_all_member">
										<div class="box-header">
											<h3 class="box-title fa fa-lock"> Data Table Aturan</h3>
											<span class="btn btn-flat btn-info pull-right fa fa-plus" data-toggle="modal" data-target="#myModal"></span>

										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="table-responsive dataTable_wrapper">

												<table class="table table-bordered table-striped" id="example1">
													<thead>
														<tr>
															<th>No</th>
															<th>ID Aturan</th>
															<th>Penyakit</th>
															<th>Gejala</th>
															<th>Probabilitas</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<?php
															$a ="select * from aturan
															join gejala on aturan.id_gejala=gejala.id_gejala
															join penyakit on aturan.id_penyakit=penyakit.id_penyakit
															order by aturan.id_aturan desc";
															$b = mysql_query($a);
															$no =1;
															while($c = mysql_fetch_array($b)){
																?>
																<td><?php echo $no; ?></td>
																<td><a href="#"><b><?php echo $c['id_aturan'];?></b></a></td>
																<td><?php echo $c['nama_penyakit']; ?></td>
																<td><?php echo $c['nama_gejala']; ?></td>
																<td><?php echo $c['probabilitas']; ?></td>
																<td>
																	<a href="index.php?view=edit_aturan&id_aturan=<?php echo $c['id_aturan'];?>" title="Edit"><i class="fa fa-edit"></i></a>
																	&nbsp;&nbsp;
																	<a onclick="return confirm('Are you sure?')" href="index.php?view=del_aturan&id_aturan=<?php echo $c['id_aturan'];?>" title="Delete"><i class="fa fa-times"></i></a>

																</td>
															</tr>
															<?php
															$no++;
														}
														?>
													</tbody>

												</table>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
									<!-- /.box -->
								</div>
								<!-- /.col -->

								<div class="col-md-4">
									<!-- Info Boxes Style 2 -->        
									<!-- PRODUCT LIST -->
									<div class="box box-info">
										<div class="box-header with-border">
											<span class="fa fa-bar-chart-o"></span><h3 class="box-title">Statistik of Aturan Data</h3>

											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
												</button>
												<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
											</div>
										</div>
										<div class="info-box bg-yellow">
											<span class="info-box-icon"><i class="fa fa-lock"></i></span>
											<div class="info-box-content">
												<?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
												$a = "select * from aturan"; $b = mysql_query($a); $rating = mysql_num_rows($b);
												$a1 = "select * from aturan where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

												?>
												<span class="progress-description"> <b><?php echo $rating;?></b> Data Aturan</span>
												<span class="progress-description"><?php echo $persen;?> New Data Todays</span>
												<!-- The progress section is optional -->
												<div class="progress">
													<div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
												</div>
												<span class="progress-description">
													<?php echo $persen/$rating*100;?>% Increase Todays
												</span>
											</div><!-- /.info-box-content -->
										</div><!-- /.info-box -->
									</div>
									<!-- /.col -->
									<div class="box box-primary">
										<div class="box-header with-border">
											<h3 class="box-title fa fa-lock"> Recently Added Aturan</h3>

											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
												</button>
												<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
											</div>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<ul class="products-list product-list-in-box">
												<?php
												$a ="select * from aturan order by id_aturan desc limit 6";
												$b = mysql_query($a);
												while($c = mysql_fetch_array($b)){?>

													<li class="item">
														<div class="product-img">
															<img src="dist/img/default-50x50.gif" alt="Product Image">
														</div>
														<div class="product-info">
															<a href="index.php?view=edit_aturan&id_aturan=<?php echo $c['id_aturan'];?>" class="product-title"><?php echo $c['id_aturan'];?>
																<span class="label label-warning pull-right"><?php echo date_format(date_create($c['waktu']), 'D,d M Y' );?> </span></a>
																<span class="product-description">
																	'<?php echo $c['id_aturan'];?>'
																</span>
															</div>
														</li>
														<?php }?>
														<!-- /.item -->
													</ul>
												</div>
												<!-- /.box-body -->
												<!-- /.box-footer -->
											</div>
											<!-- /.box -->
										</div>
										<!-- /.col -->

									</div>
									<!-- /.row -->
								</section>
								<!-- /.content -->
							</div>
							<!-- /.content-wrapper -->

							<script>
								$(function () {
									$("#example1").DataTable();
								});
							</script>





							<?php } else { ?>

								<?php include_once 'inc/side/side_menu_user.php' ;?>

								<div class="content-wrapper">
									<!-- Content Header (Page header) -->
									<!-- Content Header (Page header) -->
									<section class="content-header">
										<h1>
											<span class="fa fa-lock"></span> Dashboard Aturan
										</h1>
										<ol class="breadcrumb">
											<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
											<li class="active">Aturan</li>
										</ol>
									</section>
									<!-- Main content -->
									<section class="content">
										<!-- Info boxes -->
										<!-- Main row -->
										<div class="row">
											<!-- Left col -->
											<div class="col-md-8">

												<div class="error-page">
													<h2 class="headline text-yellow"> 404</h2>

													<div class="error-content">
														<h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

														<p>
															We could not find the page you were looking for.
															Meanwhile, you may <a href="index.php?view=home&status=not_found_page">return to HOME</a> or try using the search form.
														</p>

														<form class="search-form">
															<div class="input-group">
																<input type="text" name="search" class="form-control" placeholder="Search">

																<div class="input-group-btn">
																	<button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
																	</button>
																</div>
															</div>
															<!-- /.input-group -->
														</form>
													</div>
													<!-- /.error-content -->
												</div>
												<!-- /.error-page -->
											</div>
										</div>
									</section>
								</div>

								<?php } ?>
