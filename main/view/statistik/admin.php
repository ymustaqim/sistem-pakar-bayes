 <!--
  <?php 
if ($_SESSION['level']=='admin') { ?>
<?php include_once 'inc/side/side_menu.php' ;?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <span class="fa fa-bar-chart-o"></span> Dashboard Statistik
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Statistik</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
   <!-- Info boxes -->
   <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Data Pakar</span>
          <?php 
          $a = "select * from user_pakar";
          $b = mysql_query($a);
          $rating = mysql_num_rows($b);

          ?>
          <span class="info-box-number"><?php echo $rating;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-hospital-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Data Gejala</span>
          <?php 
          $a = "select * from gejala";
          $b = mysql_query($a);
          $review = mysql_num_rows($b);

          ?>
          <span class="info-box-number"><?php echo $review;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-medkit"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Data Penyakit</span>
          <?php 
          $a = "select * from penyakit";
          $b = mysql_query($a);
          $pesanan = mysql_num_rows($b);

          ?>
          <span class="info-box-number"><?php echo $pesanan;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-lock"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Aturan</span>
          <?php 
          $a = "select * from aturan";
          $b = mysql_query($a);
          $c = mysql_num_rows($b);

          ?>
          <span class="info-box-number"><?php echo $c;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Info boxes -->
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-desktop"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Data OS</span>
          <?php 
          $a = "select os from statistik";
          $b = mysql_query($a);
          $rating = mysql_num_rows($b);

          ?>
          <span class="info-box-number"><?php echo $rating;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-windows"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Windows</span>
          <?php 
          $a = "select os from statistik where os='Windows 8' or os='Windows 7'";
          $b = mysql_query($a);
          $review = mysql_num_rows($b);

          ?>
          <span class="info-box-number"><?php echo $review;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-linux"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">linux</span>
          <?php 
          $a = "select os from statistik where os='Linux' or os='Android'";
          $b = mysql_query($a);
          $pesanan = mysql_num_rows($b);

          ?>
          <span class="info-box-number"><?php echo $pesanan;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-apple"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Apple</span>
          <?php 
          $a = "select os from statistik where os='iPhone' or os='Mac OS x' or os='Mac OS 9' ";
          $b = mysql_query($a);
          $c = mysql_num_rows($b);

          ?>
          <span class="info-box-number"><?php echo $c;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <!-- Info boxes -->
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-12">
      <!-- /.row -->

      <div class="box" id="view_all_member">
        <div class="box-header with-border">
          <h3 class="box-title fa fa-bar-chart-o"> Data Statistik Pengguna Aplikasi</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive dataTable_wrapper">

           <table class="table no-margin table-striped" id="example1">
            <thead>
              <tr>
                <th>No.</th>
                <th>IP Address</th>
                <th>Operating System</th>
                <th>Browser</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>

              <?php
              $a ="SELECT * FROM statistik order by id_statistik desc";
              $b = mysql_query($a);
              $no =1;
              while($c = mysql_fetch_array($b)){?>
                <tr>
                  <td><?php echo $no;?></td>
                  <td><a href="#"><b><?php echo $c['ip'];?></b></a></td>
                  <td><b>
                    <?php if ($c['os']=='Windows 8'|| $c['os']=='Windows 7') { ?>

                      <i class="fa fa-windows text-info"></i> <?php echo $c['os'];?>

                      <?php } elseif ($c['os']=='Mac OS X' || $c['os']=='Mac OS 9' || $c['os']=='iPhone') {?>

                        <i class="fa fa-apple text-yellow"></i> <?php echo $c['os'];?>

                        <?php }elseif ($c['os']=='Android') {?>
                         <i class="fa fa-android text-red"></i> <?php echo $c['os'];?>

                         <?php }elseif ($c['os']=='Linux') {?>
                           <i class="fa fa-linux text-red"></i> <?php echo $c['os'];?>

                           <?php} else {?>
                             <i class="fa fa-question-circle"></i> <?php echo $c['os'];?>

                             <?php } ?>
                           </b></td>
                           <td><b><?php echo $c['browser'];?></b></td>
                           <td><b><?php echo date_format(date_create($c['hari']), 'D,d M Y' );?></b> at <b><?php echo $c['jam'];?></b></td>
                         </tr>
                         <?php
                         $no++;
                       }
                       ?>


                     </tbody>
                   </table>
                 </div>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
           <!-- /.col -->

         </div>
         <!-- /.row -->
       </section>
       <!-- /.content -->
     </div>
     <!-- /.content-wrapper -->

     <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>


            <?php } else { ?>

              <?php include_once 'inc/side/side_menu_user.php' ;?>

              <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <!-- Content Header (Page header) -->
                <section class="content-header">
                  <h1>
                    <span class="fa fa-lock"></span> Dashboard Aturan
                  </h1>
                  <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Aturan</li>
                  </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                  <!-- Info boxes -->
                  <!-- Main row -->
                  <div class="row">
                    <!-- Left col -->
                    <div class="col-md-8">

                      <div class="error-page">
                        <h2 class="headline text-yellow"> 404</h2>

                        <div class="error-content">
                          <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

                          <p>
                            We could not find the page you were looking for.
                            Meanwhile, you may <a href="index.php?view=home&status=not_found_page">return to HOME</a> or try using the search form.
                          </p>

                          <form class="search-form">
                            <div class="input-group">
                              <input type="text" name="search" class="form-control" placeholder="Search">

                              <div class="input-group-btn">
                                <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </div>
                            </div>
                            <!-- /.input-group -->
                          </form>
                        </div>
                        <!-- /.error-content -->
                      </div>
                      <!-- /.error-page -->
                    </div>
                  </div>
                </section>
              </div>

              <?php } ?>

              -->

