    <?php include_once 'inc/side/side_menu.php' ;?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <span class="fa fa-check-square-o"></span> Dashboard Detail Check
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Detail Check</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-8">
                    <?php
                    error_reporting(0);
                    $data=$_GET['status'];

                    if ($data=='success') { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check-square-o"></i> Good!</h4>                    
                            You successfully add all data to the Apps.
                        </div>
                        <?php } elseif ($data=='fail') { ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Alert!</h4>                  
                                Danger alert preview. This alert is dismissable. You can't let the form is null or no data.
                            </div>
                            <?php } else { ?>

                                <?php }
                                ?>
                                <?php
                                $id_diagnosa=$_GET['id_diagnosa'];
                                $pp = mysql_query(
                                    "select * from penyakit 
                                    join diagnosa on diagnosa.f_id_penyakit=penyakit.id_penyakit
                                    where diagnosa.id_diagnosa='$id_diagnosa'
                                    ");
                                $ppdata = mysql_fetch_array($pp);
                                ?>
                                <div class="callout callout-info">
                                    <h4>Hy, <?php echo $_SESSION['username'];?>.</h4>
                                    Bellow is the result of detail diagnosis, check this out.
                                </div>

                                <div class="box box-info" id="view_all_member">
                                    <div class="box-header">
                                        <h3 class="box-title fa fa-bar-chart-o"> Detail Diagnosis</i></h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="table-responsive dataTable_wrapper">

                                            <table class="table table-bordered table-striped" id="example1">
                                                <thead>
                                                    <tr>                                                    
                                                        <th>Nama Latin '<?php echo $ppdata['nama_penyakit'];?>'</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align:justify;"><?php echo $ppdata['nama_latin'];?></td>
                                                        
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table class="table table-bordered table-striped" id="example1">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">Riwayat Penyakit '<?php echo $ppdata['nama_penyakit'];?>'</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" style="text-align:justify;"><?php echo $ppdata['riwayat'];?></td> 
                                                    </tr>
                                                </tbody>

                                            </table>


                                            <table class="table table-bordered table-striped" id="example1">
                                                <thead>
                                                    <tr>                   
                                                        <th>Pengendalian '<?php echo $ppdata['nama_penyakit'];?>'</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align:justify;"><?php echo $ppdata['pengendalian'];?></td>
                                                        
                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- col md 8 -->
                            <div class="col-md-4">
                                <!-- Info Boxes Style 2 -->        
                                <!-- PRODUCT LIST -->
                                <div class="box box-info">
                                    <div class="box-header with-border">
                                        <span class="fa fa-bar-chart-o"></span><h3 class="box-title">Statistik of Penyakit Data</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="info-box bg-aqua">
                                        <span class="info-box-icon"><i class="fa fa-medkit"></i></span>
                                        <div class="info-box-content">
                                            <?php 
                                            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
                                            $a = "select * from penyakit"; $b = mysql_query($a); $rating = mysql_num_rows($b);
                                            $a1 = "select * from penyakit where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

                                            ?>
                                            <span class="progress-description"> <b><?php echo $rating;?></b> Data Penyakit</span>
                                            <span class="progress-description"><?php echo $persen;?> New Data Todays</span>

                                            <!-- The progress section is optional -->
                                            <div class="progress">
                                                <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
                                            </div>
                                            <span class="progress-description">
                                                <?php echo $persen/$rating*100;?>% Increase Todays
                                            </span>
                                        </div><!-- /.info-box-content -->
                                    </div><!-- /.info-box -->
                                </div>
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title fa fa-medkit"> Recently Added Penyakit</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <ul class="products-list product-list-in-box">
                                            <?php
                                            $a ="select * from penyakit order by id_penyakit desc limit 6";
                                            $b = mysql_query($a);
                                            while($c = mysql_fetch_array($b)){?>

                                                <li class="item">
                                                    <div class="product-img">
                                                        <img src="dist/img/default-50x50.gif" alt="Product Image">
                                                    </div>
                                                    <div class="product-info">
                                                        <a href="index.php?view=edit_penyakit&id_penyakit=<?php echo $c['id_penyakit'];?>" class="product-title"><?php echo $c['nama_penyakit'];?>
                                                            <span class="label label-warning pull-right"><?php echo date_format(date_create($c['waktu']), 'D,d M Y' );?> </span></a>
                                                            <span class="product-description">
                                                                '<?php echo $c['id_penyakit'];?>'
                                                            </span>
                                                        </div>
                                                    </li>
                                                    <?php }?>
                                                    <!-- /.item -->
                                                </ul>
                                            </div>
                                            <!-- /.box-body -->
                                            <!-- /.box-footer -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </section>
                        </div> 

