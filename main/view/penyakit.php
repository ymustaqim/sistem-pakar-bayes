<?php 
if ($_SESSION['level']=='admin') { ?>
<?php include_once 'inc/side/side_menu.php' ;?>
<?php include_once 'lib/operation/penyakit/auto.php' ;?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<span class="fa fa-medkit"></span> Dashboard Penyakit
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Penyakit</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- Info boxes -->
		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<div class="col-md-8">
				<?php
				error_reporting(0);
				$data=$_GET['status'];

				if ($data=='success') { ?>
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-check-square-o"></i> Good!</h4>	                
						You successfully add all data to the Apps.
					</div>
					<?php } elseif ($data=='fail') { ?>
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h4><i class="icon fa fa-ban"></i> Alert!</h4>	                
							Danger alert preview. This alert is dismissable. You can't let the form is null or no data.
						</div>
						<?php } else { ?>

							<?php }
							?>
							
							<!-- /.box -->
							<div class="box" id="view_all_member">
								<div class="box-header">
									<h3 class="box-title fa fa-medkit"> Data Table Penyakit</h3>
									<span class="btn btn-flat btn-info pull-right fa fa-plus" data-toggle="modal" data-target="#myModal"></span>

								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<div class="table-responsive dataTable_wrapper">

										<table class="table table-bordered table-striped" id="example1">
											<thead>
												<tr>
													<th>No</th>
													<th>ID Penyakit</th>
													<th>Nama Penyakit</th>
													<th>Nilai Populasi Penyakit</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<?php
													$a ="select * from penyakit order by id_penyakit desc";
													$b = mysql_query($a);
													$no =1;
													while($c = mysql_fetch_array($b)){
														?>
														<td><?php echo $no; ?></td>
														<td><a href="#"><b><?php echo $c['id_penyakit'];?></b></a></td>
														<td><?php echo $c['nama_penyakit']; ?></td>
														<td><?php echo $c['np_populasi']; ?></td>
														<td>
															<a href="index.php?view=edit_penyakit&id_penyakit=<?php echo $c['id_penyakit'];?>" title="Edit"><i class="fa fa-edit"></i></a>
															&nbsp;&nbsp;
															<a onclick="return confirm('Are you sure?')" href="index.php?view=del_penyakit&id_penyakit=<?php echo $c['id_penyakit'];?>" title="Delete"><i class="fa fa-times"></i></a>

														</td>
													</tr>
													<?php
													$no++;
												}
												?>
											</tbody>

										</table>
									</div>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
						<!-- /.col -->

						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="box box-primary" id="place_new_penyakit">
								<div class="box-header with-border">
									<h3 class="box-title fa fa-plus-circle"> Add Penyakit</h3>
								</div>
								<!-- /.box-header -->
								<!-- form start -->
								<form role="form" action="index.php?view=add_penyakit" method="post" enctype="multipart/form-data">
									<div class="box-body">
										<input type="hidden" name="id_penyakit" value="<?php echo $kd;?>">
										<div class="form-group">
											<label for="exampleInputEmail1">Nama Penyakit</label>
											<input type="text" name="nama_penyakit" class="form-control" id="exampleInputEmail1" placeholder="Enter Nama Penyakit" required="">
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Nama Latin</label>
											<textarea name="nama_latin" class="form-control" placeholder="Enter Nama Latin" required=""></textarea>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Riwayat Penyakit</label>
											<textarea name="riwayat" class="form-control" placeholder="Enter Riwayat Penyakit" required=""></textarea>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Pengendalian</label>
											<textarea name="pengendalian" class="form-control" placeholder="Enter Pengendalain" required=""></textarea>
										</div>
										
										<div class="form-group">
											<label for="exampleInputEmail1">Nilai Populasi</label>
											<input type="text" name="np_populasi" class="form-control" id="exampleInputEmail1" placeholder="Enter Nilai Populasi" required="">
										</div>
									</div>
									<!-- /.box-body -->

									<div class="box-footer">
										<button type="submit" class="btn btn-primary">Submit</button>
										<button type="button" class="btn btn-warning pull-right" data-dismiss="modal">Close</button>

									</div>
								</form>
							</div>
							</div>
						</div>

						<div class="col-md-4">
							<!-- Info Boxes Style 2 -->        
							<!-- PRODUCT LIST -->
							<div class="box box-info">
								<div class="box-header with-border">
									<span class="fa fa-bar-chart-o"></span><h3 class="box-title">Statistik of Penyakit Data</h3>

									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
									</div>
								</div>
								<div class="info-box bg-aqua">
									<span class="info-box-icon"><i class="fa fa-medkit"></i></span>
									<div class="info-box-content">
										<?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
										$a = "select * from penyakit"; $b = mysql_query($a); $rating = mysql_num_rows($b);
										$a1 = "select * from penyakit where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

										?>
										<span class="progress-description"> <b><?php echo $rating;?></b> Data Penyakit</span>
										<span class="progress-description"><?php echo $persen;?> New Data Todays</span>

										<!-- The progress section is optional -->
										<div class="progress">
											<div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
										</div>
										<span class="progress-description">
											<?php echo $persen/$rating*100;?>% Increase Todays
										</span>
									</div><!-- /.info-box-content -->
								</div><!-- /.info-box -->
							</div>
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title fa fa-medkit"> Recently Added Penyakit</h3>

									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
									</div>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<ul class="products-list product-list-in-box">
										<?php
										$a ="select * from penyakit order by id_penyakit desc limit 6";
										$b = mysql_query($a);
										while($c = mysql_fetch_array($b)){?>

											<li class="item">
												<div class="product-img">
													<img src="dist/img/default-50x50.gif" alt="Product Image">
												</div>
												<div class="product-info">
													<a href="index.php?view=edit_penyakit&id_penyakit=<?php echo $c['id_penyakit'];?>" class="product-title"><?php echo $c['nama_penyakit'];?>
														<span class="label label-warning pull-right"><?php echo date_format(date_create($c['waktu']), 'D,d M Y' );?> </span></a>
														<span class="product-description">
															'<?php echo $c['id_penyakit'];?>'
														</span>
													</div>
												</li>
												<?php }?>
												<!-- /.item -->
											</ul>
										</div>
										<!-- /.box-body -->
										<!-- /.box-footer -->
									</div>
									<!-- /.box -->
								</div>
								<!-- /.col -->

							</div>
							<!-- /.row -->
						</section>
						<!-- /.content -->
					</div>
					<!-- /.content-wrapper -->

					<script>
						$(function () {
							$("#example1").DataTable();
						});
					</script>



						<?php } else { ?>

							<?php include_once 'inc/side/side_menu_user.php' ;?>

							<div class="content-wrapper">
								<!-- Content Header (Page header) -->
								<!-- Content Header (Page header) -->
								<section class="content-header">
									<h1>
										<span class="fa fa-lock"></span> Dashboard Aturan
									</h1>
									<ol class="breadcrumb">
										<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
										<li class="active">Aturan</li>
									</ol>
								</section>
								<!-- Main content -->
								<section class="content">
									<!-- Info boxes -->
									<!-- Main row -->
									<div class="row">
										<!-- Left col -->
										<div class="col-md-8">

											<div class="error-page">
												<h2 class="headline text-yellow"> 404</h2>

												<div class="error-content">
													<h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

													<p>
														We could not find the page you were looking for.
														Meanwhile, you may <a href="index.php?view=home&status=not_found_page">return to HOME</a> or try using the search form.
													</p>

													<form class="search-form">
														<div class="input-group">
															<input type="text" name="search" class="form-control" placeholder="Search">

															<div class="input-group-btn">
																<button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
																</button>
															</div>
														</div>
														<!-- /.input-group -->
													</form>
												</div>
												<!-- /.error-content -->
											</div>
											<!-- /.error-page -->
										</div>
									</div>
								</section>
							</div>

							<?php } ?>
