<?php 
if ($_SESSION['level']=='admin') { ?>
<?php include_once 'inc/side/side_menu.php' ;?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <span class="fa fa-file-word-o"></span> Dashboard Laporan
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Laporan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">    
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-12">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title"><span class="fa fa-file-word-o"></span> Laporan Data Basis Aturan per Gejala</h3>
            <div class="box-tools pull-right">
              <?php $a = "select * from gejala"; $b = mysql_query($a); $pengguna = mysql_num_rows($b); ?>

              <span class="label label-danger"><?php echo $pengguna;?> Laporan</span>
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
           <div class="table-responsive dataTable_wrapper">
            <table class="table no-margin table-striped" id="example1">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Penyakit</th>
                  <th>Nama Penyakit</th>
                  <th>Gejala</th>
                </tr>
              </thead>
              <tbody>

                <?php
                $aa ="SELECT * FROM penyakit";
                $bb = mysql_query($aa);
                $no1 =1;
                while($cc = mysql_fetch_array($bb)){?>
                  <tr>
                    <td><?php echo $no1;?></td>
                    <td><a href="#"><b><?php echo $cc['id_penyakit'];?></b></a></td>
                    <td><b><?php echo $cc['nama_penyakit'];?></b>
                    <i><p>Note : Gejala dengan probabilitas 0.01 
                    <p>bukan termasuk gejala penyakit 
                    <p>'<b><?php echo $cc['nama_penyakit'];?></b>' </i>
                    </td>
                    <td>

                     <table class="table no-margin table-striped">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>ID Gejala</th>
                          <th>Nama Gejala</th>
                          <th>Probabilitas</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php
                        $a ="SELECT * FROM aturan
                        join penyakit on aturan.id_penyakit=penyakit.id_penyakit
                        join gejala on aturan.id_gejala=gejala.id_gejala
                        where penyakit.id_penyakit='$cc[id_penyakit]'
                        ";
                        $b = mysql_query($a);
                        $no =1;
                        while($c = mysql_fetch_array($b)){?>
                          <tr>
                            <td><?php echo $no;?></td>
                            <td><a href="#"><b><?php echo $c['id_gejala'];?></b></a></td>
                            <td><b><?php echo $c['nama_gejala'];?></b></td>
                            <td><b><?php echo $c['probabilitas'];?></b></td>
                            <td>

                            </td>
                          </tr>
                          <?php
                          $no++;
                        }
                        ?>
                      </tbody>
                    </table>

                  </td>
                </tr>
                <?php
                $no1++;
              }
              ?>


            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>

<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>

            <?php } else { ?>

              <?php include_once 'inc/side/side_menu_user.php' ;?>

              <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <!-- Content Header (Page header) -->
                <section class="content-header">
                  <h1>
                    <span class="fa fa-lock"></span> Dashboard Aturan
                  </h1>
                  <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Aturan</li>
                  </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                  <!-- Info boxes -->
                  <!-- Main row -->
                  <div class="row">
                    <!-- Left col -->
                    <div class="col-md-8">

                      <div class="error-page">
                        <h2 class="headline text-yellow"> 404</h2>

                        <div class="error-content">
                          <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

                          <p>
                            We could not find the page you were looking for.
                            Meanwhile, you may <a href="index.php?view=home&status=not_found_page">return to HOME</a> or try using the search form.
                          </p>

                          <form class="search-form">
                            <div class="input-group">
                              <input type="text" name="search" class="form-control" placeholder="Search">

                              <div class="input-group-btn">
                                <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
                                </button>
                              </div>
                            </div>
                            <!-- /.input-group -->
                          </form>
                        </div>
                        <!-- /.error-content -->
                      </div>
                      <!-- /.error-page -->
                    </div>
                  </div>
                </section>
              </div>

              <?php } ?>
