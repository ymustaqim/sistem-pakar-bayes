    <?php include_once 'inc/side/side_menu_user.php' ;?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <span class="fa fa-check-square-o"></span> Dashboard Detail Check
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Detail Check</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12">
                    <?php
                    error_reporting(0);
                    $data=$_GET['status'];

                    if ($data=='success') { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check-square-o"></i> Good!</h4>                    
                            You successfully add all data to the Apps.
                        </div>
                        <?php } elseif ($data=='fail') { ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Alert!</h4>                  
                                Danger alert preview. This alert is dismissable. You can't let the form is null or no data.
                            </div>
                            <?php } else { ?>

                                <?php }
                                ?>
                                <?php
                                $id_diagnosa=$_GET['id_diagnosa'];
                                $pp = mysql_query(
                                    "select * from penyakit 
                                    join diagnosa on diagnosa.f_id_penyakit=penyakit.id_penyakit
                                    where diagnosa.id_diagnosa='$id_diagnosa'
                                    ");
                                $ppdata = mysql_fetch_array($pp);
                                ?>
                                <div class="callout callout-info">
                                    <h4>Hy, <?php echo $_SESSION['username'];?>! Welcome to PiBa Apps.</h4>
                                    Bellow is probability of detail diagnosis, check this out.
                                </div>

                                <div class="box box-info" id="view_all_member">
                                    <div class="box-header">
                                        <h3 class="box-title fa fa-bar-chart-o"> Detail Diagnosis</i></h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="table-responsive dataTable_wrapper">

                                            <table class="table table-bordered table-striped" id="example1">
                                                <thead>
                                                    <tr>                                                    
                                                        <th>Nama Latin '<?php echo $ppdata['nama_penyakit'];?>'</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align:justify;"><?php echo $ppdata['nama_latin'];?></td>
                                                        
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table class="table table-bordered table-striped" id="example1">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">Riwayat Penyakit '<?php echo $ppdata['nama_penyakit'];?>'</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2" style="text-align:justify;"><?php echo $ppdata['riwayat'];?></td> 
                                                    </tr>
                                                </tbody>

                                            </table>


                                            <table class="table table-bordered table-striped" id="example1">
                                                <thead>
                                                    <tr>                   
                                                        <th>Pengendalian '<?php echo $ppdata['nama_penyakit'];?>'</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align:justify;"><?php echo $ppdata['pengendalian'];?></td>
                                                        
                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- col md 8 -->

                        </div>
                    </section>
                </div> 

