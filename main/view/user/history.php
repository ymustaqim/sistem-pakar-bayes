<?php include_once 'inc/side/side_menu_user.php' ;?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"  id="top">
    <!-- Content Header (Page header) -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <span class="fa fa-check-square-o"></span> Dashboard History
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">History</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
                <?php
                error_reporting(0);
                $data=$_GET['status'];

                if ($data=='success') { ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check-square-o"></i> Good!</h4>                    
                        You successfully add all data to the Apps.
                    </div>
                    <?php } elseif ($data=='fail') { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>                  
                            Danger alert preview. This alert is dismissable. You can't let the form is null or no data.
                        </div>
                        <?php } else { ?>

                            <?php }
                            ?>
                            <?php if ($_GET['id_diagnosa']=='') { ?>

                                <?php } else { ?>
                                    <div class="box box-info">
                                        <div class="box-header">
                                            <h3 class="box-title fa fa-bar-chart-o"> Data Table History for your Diagnosis</h3>
                                        </div>
                                        <div class="box-body">
                                            <div class="table-responsive dataTable_wrapper">

                                                <table class="table table-bordered table-striped" id="example1">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Gejala ID</th>
                                                            <th>Nama Gejala</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <?php
                                                            $user = $_SESSION['id_pakar'];
                                                            $a ="select * from diagnosa

                                                            join diagnosa_detail on diagnosa_detail.id_diagnosa=diagnosa.id_diagnosa

                                                            join user_pakar on diagnosa.id_pakar=user_pakar.id_pakar

                                                            join aturan on diagnosa_detail.id_aturan=aturan.id_aturan

                                                            join gejala on aturan.id_gejala=gejala.id_gejala

                                                            join penyakit on aturan.id_penyakit=penyakit.id_penyakit

                                                            where diagnosa.id_diagnosa='$_GET[id_diagnosa]' and user_pakar.id_pakar='$user' group by gejala.nama_gejala";

                                                            $b = mysql_query($a);
                                                            $no =1;
                                                            $hasilbagi=1;
                                                            while($c = mysql_fetch_array($b)){
                                                                ?>
                                                                <td><?php echo $no; ?></td>
                                                                <td><?php echo $c['id_gejala']; ?></td>
                                                                <td><?php echo $c['nama_gejala']; ?></td>                                                        
                                                            </tr>
                                                            <?php
                                                            $no++;
                                                        }
                                                        ?>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box -->
                                    <?php } ?>

                                    <?php if ($_GET['id_diagnosa']=='') { ?>

                                        <?php } else { ?>
                                            <div class="callout callout-info">
                                                <h4>Hy, <?php echo $_SESSION['username'];?>.</h4>
                                                Bellow is your probability of your diagnosis, check this out.
                                            </div>
                                            <?php } ?>

                                            <?php 
                                            $id_aturan=$_GET['id_diagnosa'];
                                            $data_p = mysql_query("SELECT * from aturan
                                                join penyakit on aturan.id_penyakit=penyakit.id_penyakit
                                                join gejala on aturan.id_gejala=gejala.id_gejala
                                                join diagnosa_detail on diagnosa_detail.id_aturan=aturan.id_aturan
                                                join diagnosa on diagnosa_detail.id_diagnosa=diagnosa.id_diagnosa
                                                where diagnosa_detail.id_diagnosa='$id_aturan'
                                                group by penyakit.id_penyakit
                                                ;");
                                            $nilai_index = 1; ?>
                                                <script type="text/javascript">
                                                    window.totalDataP = '<?php echo mysql_num_rows($data_p) ?>';
                                                </script>
                                           <?php while ($diag=mysql_fetch_array($data_p)) { ?>

                                                <div class="box box-warning" id="view_all_member">
                                                    <div class="box-header">
                                                        <h3 class="box-title fa fa-medkit"> <?php echo $diag['id_penyakit'];?> / <?php echo $diag['nama_penyakit'];?></h3>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="table-responsive dataTable_wrapper">

                                                            <table class="table table-bordered table-striped" id="example1">
                                                                <thead>
                                                                    <tr>
                                                                        <th>No</th>
                                                                        <th>Gejala ID</th>
                                                                        <th>Nama Gejala</th>
                                                                        <th>Probabilitas</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <?php
                                                                        $user = $_SESSION['id_pakar'];
                                                                        $a ="select * from diagnosa

                                                                        join diagnosa_detail on diagnosa_detail.id_diagnosa=diagnosa.id_diagnosa

                                                                        join user_pakar on diagnosa.id_pakar=user_pakar.id_pakar

                                                                        join aturan on diagnosa_detail.id_aturan=aturan.id_aturan

                                                                        join gejala on aturan.id_gejala=gejala.id_gejala

                                                                        join penyakit on aturan.id_penyakit=penyakit.id_penyakit

                                                                        where diagnosa.id_diagnosa='$_GET[id_diagnosa]' 

                                                                        and user_pakar.id_pakar='$user' 

                                                                        and penyakit.id_penyakit='$diag[id_penyakit]'
                                                                        
                                                                        ";
                                                                        $b = mysql_query($a);
                                                                        $no =1;
                                                                        $hasil=1;
                                                                        while($c = mysql_fetch_array($b)){
                                                                            ?>
                                                                            <td><?php echo $no; ?></td>
                                                                            <td><?php echo $c['id_gejala']; ?></td>
                                                                            <td><?php echo $c['nama_gejala']; ?></td>
                                                                            <td><?php echo $c['probabilitas']; ?></td>                                                        
                                                                        </tr>
                                                                        <?php
                                                            //HITUNG NILAI PROBABILITAS KESELURUHAN
                                                                        $hasil=$hasil*$c['probabilitas'];
                                                                        $no++;
                                                                    }
                                                                    ?>

                                                                    <tr>
                                                                        <td colspan="3" align="center"><b>Hasil p(<?php echo $diag['nama_gejala'];?>, ...,m)</b></td>
                                                                        <td>
                                                                            <b>
                                                                                <?php 
                                                                    //TAMPIL HASIL PROBABILITASNYA
                                                                                echo $hasil;
                                                                                ?>
                                                                            </b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" align="center"><b>Populasi Penyakit <?php echo $diag['id_penyakit'];?> (<?php echo $diag['nama_penyakit'];?>)</b></td>
                                                                        <td>
                                                                            <b>
                                                                               <?php echo $diag['np_populasi'];?> 
                                                                           </b>
                                                                       </td>
                                                                   </tr>
                                                               </tbody>
                                                           </table>
                                                       </div>
                                                   </div>

                                                   <div class="box-footer">
                                                    <!--TAMPIL NILAI-->
                                                    <h3 class="box-title fa fa-info-circle"><b> 
                                                     p (<?php echo $diag['id_penyakit'];?> | <?php echo $diag['id_gejala'];?>) x .. p(<?php echo $diag['id_penyakit'];?> | GJLAm) : <?php echo $hasil;?>*<?php echo $diag['np_populasi'];?> = 
                                                     <i><u id="nilai_<?php echo $nilai_index ?>">

                                                        <?php $sum = $hasil*$diag['np_populasi']; 
                                                            //echo $kalihasil = sprintf('%f', $sum);
                                                        echo $kalihasil = number_format($sum, 20);

                                                        ?>
                                                        
                                                    </u></i></b></h3>
                                                </div>
                                                <?php $nilai_index++ ?>

                                                <!--BUAT ARRAY DISINI UNTUK MENENTUKAN NILAI TERBESAR-->
                                                <?php
                                       //$id =$diag['id_penyakit'];                                        
                                                $id .=$diag['id_penyakit'].',';
                                                $nilai .= trim($kalihasil.',');
                                                $datah .=$kalihasil."-".$diag['id_penyakit']."-".$diag['nama_penyakit'].',';
                                                $datas .=$kalihasil."-".$diag['id_penyakit'].',';
                                       //print_r($datah);
                                                ?>


                                            </div>
                                            <!--END LOOPING DISINI-->
                                            <?php }  ?>
                                            <!--UNTUK MEMISAH DATA-->
                                            <?php 
                                            $iddata = explode(',',$id,-1); 
                                            $array_nilai = explode(',', $nilai,-1);
                                            $result = array_combine($iddata,$array_nilai);
                                            $hasildata = explode(',', $datah);
                                            $hasildatas = explode(',', $datas);

                                   //ambil data max dan potong 10 character dari kanan
                                            $ambildata = substr(max($hasildatas), -10);
                                            //echo $hasildata;
                                            ?>


                                             <!--TAMPIL PEMBAGI-->
                                                <div class="callout callout-danger">
                                                    <p><b>Pembagi : 
                                                        <span id="nilai_pembagi">
                                                            <?php $bagi = array_sum($result); $bagi2 = number_format($bagi,20); echo $bagi2;?>
                                                            <script type="text/javascript">
                                                                window.Pembagi = Number('<?php echo $bagi2; ?>');
                                                            </script>
                                                            
                                                        </span></b></p>
                                                        <hr>
                                                        <p><b>Hasil Hitung</b></p>
                                                        <hr>
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Hitung</th>
                                                                    <th>Hasil</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="list_perhitungan">

                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>
                                            <?php if ($_GET['id_diagnosa']=='') { ?>
                                               <div class="callout callout-warning">
                                                   <h4>OOppssss!!! Hy, <?php echo $_SESSION['username'];?>! </h4>
                                                   It's something wrong with the data. You don't have any data yet. <p>Click <i>Start Diagnosis</i> to start your experience.
                                                   <a href="index.php?view=diXD23agdfsDFno56dcsa97KJHJJjdsa&redirect=from%20history" ><span class="btn btn-flat btn-info">Start Diagnosis</span></a> -- OR -- view your profile 
                                                   <a href="index.php?view=p768sdhfksdKJGHKLJGHKLH768djhkskJH&redirect=from%20history" ><span class="btn btn-flat btn-info">Profile</span></a> 

                                               </div>
                                               <?php } else { ?>
                                                <!--TAMPIL NILAI MAX-->
                                                <div class="box-body callout callout-info">
                                                        
                                                            <h3>Kesimpulan diambil dari yang paling besar Max (<b>
                                                            <span id="hasil_kesimpulan"></span></b>)</h3>
                                                            <h4>Dengan hasil yang sudah diketahui, kemungkinan besar user menderita penyakit <b>
                                                            [<?php echo substr(max($hasildata),23);?>] </b></h4>
                                                    </div>
                                                <div class="callout callout-warning" id="result">
                                                    <h4>Hy, <?php echo $_SESSION['username'];?>! Here is your detail diagnosis.</h4>
                                                    Click the button <i>Show More</i> for further information.
                                                    <form action="index.php?view=dlkfjas76986KJHSDFH76jhksfdjh&id_diagnosa=<?php echo $_GET['id_diagnosa'];?>&id_penyakit=<?php echo $ambildata;?>" enctype="multipart/form-data" method="post">
                                                        <input type="hidden" value="<?php echo $ambildata;?>" name="id_penyakit">
                                                        <button class="btn btn-flat btn-info" type="submit" value="submit">Show More</button> <a href="#top" class="btn btn-primary btn-flat btn-info"><b>Go to top</b></a>                                   
                                                    </form>
                                                </div>
                                                <?php } ?>


                                            </div>
                                            <!-- /.col -->

                                            <!-- DISINI COL MD 4 SAMPING -->
                                            <div class="col-md-4">

                                                      <!-- Profile Image -->
                                                      <div class="box box-primary">
                                                        <div class="box-header with-border">
                                                        <h3 class="box-title fa fa-info-circle"> Detail Post</h3>

                                                            <div class="box-tools pull-right">
                                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body box-profile">
                                                        <?php 
                                                        $di=$_GET['id_diagnosa'];
                                                        $pakar = $_SESSION['id_pakar'];
                                                        $waktu = mysql_query("select * from diagnosa join user_pakar on diagnosa.id_pakar=user_pakar.id_pakar where user_pakar.id_pakar='$pakar' and diagnosa.id_diagnosa='$di' ");
                                                        $oke = mysql_fetch_array($waktu);
                                                        ?>

                                                          <?php if ($oke['foto']=='null') {?>
                                                            <img class="profile-user-img img-responsive img-circle" src="dist/img/default.png" alt="<?php echo $oke['username'];?>">
                                                            <?php } else { ?>
                                                                <img class="profile-user-img img-responsive img-circle" src="file/gambar/user/<?php echo $oke['foto'];?>" alt="<?php echo $oke['username'];?>">
                                                                <?php } ?>

                                                                <h3 class="profile-username text-center"><?php echo $oke['nama'];?></h3>

                                                                <ul class="list-group list-group-unbordered">
                                                                    <li class="list-group-item">
                                                                      <b>Posted by </b> <a class="pull-right"><?php echo $oke['nama'];?></a>
                                                                  </li>
                                                                  <li class="list-group-item">
                                                                      <b>Date</b> <a class="pull-right"><?php echo date_format(date_create($oke['waktu_diagnosa']), 'D, d M Y' );?></a>
                                                                  </li>
                                                                  <li class="list-group-item">
                                                                      <b>Time</b> <a class="pull-right"><?php echo $oke['jam'];?></a>
                                                                  </li>
                                                              </ul>

                                                              <a href="#result" class="btn btn-primary btn-flat btn-info"><b>Go to Result</b></a>
                                                          </div>
                                                          <!-- /.box-body -->
                                                      </div>
                                                      <!-- /.box -->
                                                  </div>
                                                  <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </section>
                                    <!-- /.content -->
                                </div>
                                <!-- /.content-wrapper -->

                                 <script type="text/javascript">

                                            function hitungnilai() {
                                                var tot = Number(window.totalDataP);
                                                window.arrayNilai = [];

                                                for (var i = 1; i <= tot; i++) {
                                                    var data = Number($("#nilai_" + i).html());
                                                    window.arrayNilai.push( data );
                                                }


                                                var list = '';
                                                window.kesimpulanNilai = [];

                                                $.each(window.arrayNilai , function (index, value){
                                                    list += '<tr>'+
                                                    '<td>' + (index+1) + '</td>' +
                                                    '<td>' + Number(value) + ' / ' + window.Pembagi + ' </td>' +
                                                    '<td>' + Number(value)  /  window.Pembagi + ' </td>' +
                                                    '</tr>';

                                                    var kes = Number(value)  /  window.Pembagi;
                                                    window.kesimpulanNilai.push(kes);
                                                });
        
                                                $("#list_perhitungan").html(list);



                                                //hasil kesimpulan
                                                Array.prototype.max = function() {
                                                  return Math.max.apply(null, this);
                                                };

                                                //ambil kesimpulan terbesar
                                                window.kesimpulanTerbesar = window.kesimpulanNilai.max();

                                                $.ajax({

                                                  url: "lib/operation/diagnosa/ajax_get_hasil_penyakit.php",
                                                  //GET URL INI MASIH ERROR -_-

                                                  data:{
                                                        id_diagnosa:'<?php echo $_GET["id_diagnosa"];?>',
                                                        hasil_diagnosa:window.kesimpulanTerbesar
                                                  },
                                                  success:function(res){
                                                    $("#hasil_penyakit").html(res);

                                                  }
                                                });

                                               

                                                $("#hasil_kesimpulan").html(window.kesimpulanNilai.max());
                                                //end hasil kesimpulan
                                            }

                                            hitungnilai();

                                        </script>

