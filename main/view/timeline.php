<?php include_once 'inc/side/side_menu.php' ;?>
<?php
$user = $_SESSION['id_pakar'];
$a = mysql_query("select * from user_pakar where id_pakar='$user' ");
$c = mysql_fetch_array($a);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<span class="fa fa-users"></span> Dashboard Member
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Member</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Info boxes -->
		<!-- Main row -->
		<div class="row">
			<div class="col-md-8">	
				<div class="callout callout-info">
                    <h4>Hy, <?php echo $_SESSION['username'];?>! Welcome to Sistem Cerdas Apps.</h4>
                    Start your Diagnosis with this button and share to your friends. 
                    <?php             
                     $a1 = "select * from diagnosa where waktu_diagnosa=curdate() and id_pakar='$_SESSION[id_pakar]' "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1); ?>
                     <?php if ($persen!=0) {?>
                         Good news, you post <?php echo $persen;?> data todays. View your diagnosa <a href="index.php?view=profile"> <button type="button" class="btn bg-maroon btn-flat margin">Profile</button></a>  -OR- post new data <a href="index.php?view=diagnosa"> <button type="button" class="btn bg-maroon btn-flat margin">Start Diagnosis !</button></a>
                     <?php } else { ?>
                        Bad news, you dont have post yet todays. <a href="index.php?view=diagnosa"> <button type="button" class="btn bg-maroon btn-flat margin">Start Diagnosis !</button></a>
                    <?php } ?>
                </div>			
                <div class="nav-tabs-custom">
                 <ul class="nav nav-tabs">
                     <li class="active"><a href="#timeline" data-toggle="tab">Timeline</a></li>
                 </ul>
                 <div class="tab-content">
                  <div class="active tab-pane" id="timeline">
                   <!-- The timeline -->

                    <!-- jenis looping yang digunanakan :
                    1. looping perhari untuk menampilkan data per hari
                    2. looping per id_diagnosa 
                    3. looping data per id_diagnosa 
                    4. looping per hari harus dibedakan dengan looping yang berbeda
                -->
                <?php
                include_once 'view/timeline/today.php';                       
                include_once 'view/timeline/yesterday.php';
                ?>


            </div>

        </div>
        <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
</div>
<!-- /.col -->
<div class="col-md-4">
<div class="row">

  <div class="col-md-12 col-sm-6 col-xs-12">
    <div class="info-box bg-red">
      <span class="info-box-icon"><i class="fa fa-users"></i></span>
      <div class="info-box-content">
       <?php  
       $a = "select * from user_pakar"; $b = mysql_query($a); $rating = mysql_num_rows($b);
       $a1 = "select * from user_pakar where waktu_daftar=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

       ?>
       <span class="progress-description"> <b><?php echo $rating;?></b> Data Pakar</span>
       <span class="progress-description"><?php echo $persen;?> New Data Todays</span>
       <!-- The progress section is optional -->
       <div class="progress progress-sm active">
        <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
    </div>
    <span class="progress-description">
        <?php echo $persen/$rating*100;?>% Increase Todays
    </span>
</div><!-- /.info-box-content -->
</div><!-- /.info-box -->
</div>
<!-- /.col -->
</div>
<div class="row">

    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="fa fa-hospital-o"></i></span>
        <div class="info-box-content">
          <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
          $a = "select * from gejala"; $b = mysql_query($a); $rating = mysql_num_rows($b);
          $a1 = "select * from gejala where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

          ?>
          <span class="progress-description"> <b><?php echo $rating;?></b> Data Gejala</span>           
          <span class="progress-description"><?php echo $persen;?> New Data Todays</span>
          <!-- The progress section is optional -->
          <div class="progress">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
        </div>
        <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
        </span>
    </div><!-- /.info-box-content -->
</div><!-- /.info-box -->
</div>
<!-- /.col -->
</div>
<div class="row">

    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-medkit"></i></span>
        <div class="info-box-content">
          <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
          $a = "select * from penyakit"; $b = mysql_query($a); $rating = mysql_num_rows($b);
          $a1 = "select * from penyakit where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

          ?>
          <span class="progress-description"> <b><?php echo $rating;?></b> Data Penyakit</span>
          <span class="progress-description"><?php echo $persen;?> New Data Todays</span>

          <!-- The progress section is optional -->
          <div class="progress">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
        </div>
        <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
        </span>
    </div><!-- /.info-box-content -->
</div><!-- /.info-box -->
</div>
<!-- /.col -->
</div>
<div class="row">

    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="info-box bg-yellow">
        <span class="info-box-icon"><i class="fa fa-lock"></i></span>
        <div class="info-box-content">
          <?php 
            //$waktu=mysql_query("SELECT left(waktu,10) FROM user_pakar WHERE left(waktu,10) = curdate()"); 
          $a = "select * from aturan"; $b = mysql_query($a); $rating = mysql_num_rows($b);
          $a1 = "select * from aturan where waktu=curdate() "; $b1 = mysql_query($a1); $persen = mysql_num_rows($b1);

          ?>
          <span class="progress-description"> <b><?php echo $rating;?></b> Data Aturan</span>
          <span class="progress-description"><?php echo $persen;?> New Data Todays</span>
          <!-- The progress section is optional -->
          <div class="progress">
            <div class="progress-bar" style="width: <?php echo $rating;?>%"></div>
        </div>
        <span class="progress-description">
            <?php echo $persen/$rating*100;?>% Increase Todays
        </span>
    </div><!-- /.info-box-content -->
</div><!-- /.info-box -->
</div>
<!-- /.col -->

</div>
<!-- /.row -->
<div class="callout callout-info">
    <h4>Hy, <?php echo $_SESSION['username'];?>! Whats happend today?</h4>
    Start your Diagnosis with this button and share to your friends. <a href="index.php?view=diagnosa"> <button type="button" class="btn bg-maroon btn-flat margin">Start Diagnosis !</button></a>
</div>  
</div>

</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

