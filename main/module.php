<?php

defined ('qpizza') or die ();

$view = isset ($_GET['view']) ? $_GET['view']:null;
switch ($view) {
			
	case 'user_pakar':
	include_once "view/admin.php";
		break;

		//anak admin
		case 'add_admin':
		include_once "lib/operation/admin/additem.php";
			break;

		case 'del_admin':
		include_once "lib/operation/admin/delitem.php";
			break;

		case 'edit_admin':
		include_once "lib/operation/admin/edititem.php";
			break;

		case 'edit_admin_p':
		include_once "lib/operation/admin/edititem_process.php";
			break;

	case 'gejala':
	include_once "view/gejala.php";
		break;

		//anak admin
		case 'add_gejala':
		include_once "lib/operation/gejala/additem.php";
			break;

		case 'del_gejala':
		include_once "lib/operation/gejala/delitem.php";
			break;

		case 'edit_gejala':
		include_once "lib/operation/gejala/edititem.php";
			break;

		case 'edit_gejala_p':
		include_once "lib/operation/gejala/edititem_process.php";
			break;

	case 'penyakit':
	include_once "view/penyakit.php";
		break;

		//anak admin
		case 'add_penyakit':
		include_once "lib/operation/penyakit/additem.php";
			break;

		case 'del_penyakit':
		include_once "lib/operation/penyakit/delitem.php";
			break;

		case 'edit_penyakit':
		include_once "lib/operation/penyakit/edititem.php";
			break;

		case 'edit_penyakit_p':
		include_once "lib/operation/penyakit/edititem_process.php";
			break;

	case 'aturan':
	include_once "view/aturan.php";
		break;

		//anak admin
		case 'add_aturan':
		include_once "lib/operation/aturan/additem.php";
			break;

		case 'del_aturan':
		include_once "lib/operation/aturan/delitem.php";
			break;

		case 'edit_aturan':
		include_once "lib/operation/aturan/edititem.php";
			break;

		case 'edit_aturan_p':
		include_once "lib/operation/aturan/edititem_process.php";
			break;

	case 'diagnosa':
		include_once "view/diagnosa.php";
		break;

	case 'laporan':
		include_once "view/laporan.php";
		break;

	case 'history':
		include_once "view/history.php";
		break;
		//anak history
		case 'delhistory':
		include_once "lib/operation/diagnosa/delhistory.php";
		break;

		case 'history_other':
			include_once "view/history_other.php";
			break;

	case 'profile':
		include_once "view/profile.php";
		break;
		//anak
		case 'profile_other':
			include_once "view/profile_other.php";
			break;

		case 'change_profile':
			include_once "lib/operation/profile/edititem_process.php";
			break;

	// case 'statistik':
	// 	include_once "view/statistik/admin.php";
	// 	break;

	case 'check':
		include_once "lib/operation/diagnosa/check.php";
		break;
		//anak detail check
		case 'detailcheck':
			include_once "lib/operation/diagnosa/detailcheck.php";
			break;
		case 'detaildiagnosis':
			include_once "view/detailcheck.php";
			break;

	case 'timeline':
		include_once "view/timeline.php";
		break;


	//================================================================================//
		//USER CASE
		case 'diXD23agdfsDFno56dcsa97KJHJJjdsa':
			include_once "view/user/diagnosa.php";
			break;

		case 'L67p6sdfjkahKJH67588fkjasdKLJG34':
			include_once "view/user/laporan.php";
			break;

		case 'H876sdjfhkashdKJHGKLyG7687kjshdf':
			include_once "view/user/history.php";
			break;
			//anak history

			case '879dskfljsdflLKJHSF879809SDFJHHjh':
				include_once "lib/operation/diagnosa/user/delhistory.php";
				break;

			case 'sjkdfhJHDKJHFK76868dfjhJKGGH78kjhgkg':
				include_once "view/user/history_other.php";
				break;

		case 'p768sdhfksdKJGHKLJGHKLH768djhkskJH':
			include_once "view/user/profile.php";
			break;
			//anak profile
			case 'sdfkjSKJHDF876SDFKJHjhsdf876sdkjhf':
				include_once "lib/operation/profile/edititem_process_user.php";
				break;
			//profile other
			case 'fjsadlkjfs8fisdfuo8sdf04sdkjflsdfo8uyid':
				include_once "view/user/profile_other.php";
				break;

		case 'cJHHkjfdjd87687687sdfjKJGDKJ7889dj':
			include_once "lib/operation/diagnosa/user/check.php";
			break;
			//anak check
			case 'dlkfjas76986KJHSDFH76jhksfdjh':
				include_once "lib/operation/diagnosa/user/detailcheck.php";
				break;

		case '879kjsfklsjhKJHGLKJK876KJHGKGLKJGL':
			include_once "view/user/detailcheck.php";
			break;
//======================================================= end state


	case 'signout':
	include_once "lib/signout.php";
		break;


	default:
		if ($_SESSION['level']=='admin') {
			include_once "view/home.php";
		}
		else{
			include_once "view/user/home.php";
		}
				
	break;

}